package br.edu.catolicato.sigeplane.controllers.gestaoUsuarios;

import br.edu.catolicato.sigeplane.domain.Discente;
import br.edu.catolicato.sigeplane.domain.Perfil;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.gestaoUsuarios.DiscenteService;
import br.edu.catolicato.sigeplane.util.CriptografiaSHA;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 07 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Named
@ViewScoped
public class AlteracoesDiscenteController implements Serializable {
    private static final long serialVersionUID = 1L;

    @EJB
    private Repositorio repositorio;
    @Inject
    private DiscenteService discenteService;
    private List<Discente> lista;
    private List<Discente> listaFiltro;
    @Inject
    private Discente novoDiscente;
    private Discente discenteSelecionado;

    @PostConstruct
    public void init() {
        this.discenteService.setRepositorio(repositorio);
    }

    public List<Discente> getLista() {
        if (lista == null) {
            lista = discenteService.buscaTodos();
        }
        return lista;
    }

    public void setLista(List<Discente> lista) {
        this.lista = lista;
    }

    public List<Discente> getListaFiltro() {
        return listaFiltro;
    }

    public void setListaFiltro(List<Discente> listaFiltro) {
        this.listaFiltro = listaFiltro;
    }

    public Discente getNovoDiscente() {
        return novoDiscente;
    }

    public void setNovoDiscente(Discente novoDiscente) {
        this.novoDiscente = novoDiscente;
    }

    public Discente getDiscenteSelecionado() {
        return discenteSelecionado;
    }

    public void setDiscenteSelecionado(Discente discenteSelecionado) {
        this.discenteSelecionado = discenteSelecionado;
    }

    public void salvarDados() throws IllegalAccessException, InstantiationException, IOException {
        if (novoDiscente != null) {
            novoDiscente.setSenha(CriptografiaSHA.hash256(novoDiscente.getMatricula()));
            novoDiscente.setPerfil(Perfil.DISCENTE);
            boolean ret = discenteService.salvarDiscente(novoDiscente);
            if (ret) {
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Registro salvo com sucesso!");
                novoDiscente = Discente.class.newInstance();
                lista = null;
                RequestContext.getCurrentInstance().execute("PF('dlgAdicionar').hide()");
                RequestContext.getCurrentInstance().update("tabelaDiscentes");
            }
        }
    }

    public void editarDados() throws IllegalAccessException, InstantiationException {
        if (discenteSelecionado != null) {
            boolean ret = discenteService.editarDiscente(discenteSelecionado);
            if (ret) {
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Registro editado com sucesso!");
                discenteSelecionado = null;
                lista = null;
                RequestContext.getCurrentInstance().execute("PF('dlgEditar').hide()");
                RequestContext.getCurrentInstance().update("tabelaDiscentes");
                RequestContext.getCurrentInstance().update("panelMenu");
            }
        }
    }

    public void excluirDiscente() {
        if (discenteSelecionado != null) {
            boolean ret = discenteService.excluirDiscente(discenteSelecionado);
            if (ret) {
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Registro excluído com sucesso!");
                discenteSelecionado = null;
                lista = null;
                RequestContext.getCurrentInstance().update("tabelaDiscentes");
                RequestContext.getCurrentInstance().update("panelMenu");
            }
        }
    }
}
