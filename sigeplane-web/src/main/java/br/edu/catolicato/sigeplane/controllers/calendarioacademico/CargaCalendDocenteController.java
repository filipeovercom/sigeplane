package br.edu.catolicato.sigeplane.controllers.calendarioacademico;

import br.edu.catolicato.sigeplane.domain.CalendarioAcademico;
import br.edu.catolicato.sigeplane.domain.CalendarioDocente;
import br.edu.catolicato.sigeplane.importacao.ImportCalendDocente;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioAcademicoService;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 25 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Named
@ViewScoped
public class CargaCalendDocenteController implements Serializable {
    private static final long serialVersionUID = 1L;

    @EJB
    private Repositorio repositorio;
    @Inject
    private ImportCalendDocente importCalendDocente;
    @Inject
    private CalendarioAcademicoService calendarioAcademicoService;
    private List<CalendarioDocente> horarios;
    private Integer anoSemestre;
    private Integer periodoSemestre;
    private UploadedFile uploadedFile;
    private CalendarioAcademico calendSelecionado;
    private List<CalendarioAcademico> listaCalendarios;

    @PostConstruct
    public void init() {
        this.calendarioAcademicoService.setRepositorio(repositorio);
        this.importCalendDocente.setRepositorio(repositorio);
        horarios = new LinkedList<>();
    }

    public List<CalendarioDocente> getHorarios() {
        return horarios;
    }

    public void setHorarios(List<CalendarioDocente> horarios) {
        this.horarios = horarios;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public void fileUploadListener(FileUploadEvent event) throws IOException {
        uploadedFile = event.getFile();
        lerArquivo();
        RequestContext.getCurrentInstance().update("tabelaHorarios");
    }

    public void lerArquivo() throws IOException {
        if (calendSelecionado != null && uploadedFile != null) {
            horarios = importCalendDocente.lerArquivo(uploadedFile, calendSelecionado);
            if (horarios != null && horarios.size() > 0) {
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Foram lidos " + horarios.size() + " registro(s)");
            }
        } else {
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Ooops!", "Calendário Acadêmico não informado ou aconteceu algum erro ao importar seu arquivo!");
        }
    }

    public void salvarDados() {
        if (calendSelecionado != null && horarios.size() > 0) {
            boolean ret = importCalendDocente.salvarDados();
            if (ret) {
                horarios = new LinkedList<>();
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Registros salvos com sucesso!");
            }
        } else {
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Ooops!", "Calendário Acadêmico não informado ou aconteceu algum erro ao importar seu arquivo!");
        }
    }

    public Integer getAnoSemestre() {
        return anoSemestre;
    }

    public void setAnoSemestre(Integer anoSemestre) {
        this.anoSemestre = anoSemestre;
    }

    public Integer getPeriodoSemestre() {
        return periodoSemestre;
    }

    public void setPeriodoSemestre(Integer periodoSemestre) {
        this.periodoSemestre = periodoSemestre;
    }

    public CalendarioAcademico getCalendSelecionado() {
        return calendSelecionado;
    }

    public void setCalendSelecionado(CalendarioAcademico calendSelecionado) {
        this.calendSelecionado = calendSelecionado;
    }

    public List<CalendarioAcademico> getListaCalendarios() {
        if (listaCalendarios == null) {
            listaCalendarios = calendarioAcademicoService.buscaTodos();
        }
        return listaCalendarios;
    }

    public void setListaCalendarios(List<CalendarioAcademico> listaCalendarios) {
        this.listaCalendarios = listaCalendarios;
    }
}
