package br.edu.catolicato.sigeplane.controllers.seguranca;

import br.edu.catolicato.sigeplane.domain.Usuario;
import br.edu.catolicato.sigeplane.seguranca.ControleDeSeguranca;
import br.edu.catolicato.sigeplane.util.CriptografiaSHA;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.Serializable;

/**
 * Criado por Filipe D. Abreu em 26 de Junho de 2015. SIGEPLANE - Sistema de
 * Gestão de Plano de Ensino Faculdade Católica do Tocantins - Sistemas de
 * Informação
 */
@Named
@ConversationScoped
public class SegurancaController implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private ControleDeSeguranca seguranca;
    @Inject
    private Conversation conversation;
    private String matricula;
    private String senha;
    private String email;
    private Logger log;

    @PostConstruct
    public void init() {
        log = LogManager.getLogger(ControleDeSeguranca.class.getName());
        if (!conversation.isTransient()) {
            conversation.begin();
            log.debug("Inicializando Controller de Segurança");
        }
    }

    public String logar() throws IOException {
        try {
            log.debug("Logando o usuario: " + this.matricula);
            seguranca.colocaUsuarioNaSessao(this.matricula);
            FacesUtil.getRequest().login(this.matricula, this.senha);
            log.debug("Usuario logado com sucesso!");
            if (!conversation.isTransient()) {
                conversation.end();
                log.debug("Finalizando Controller de Segurança");
            }
            return "home?faces-redirect=true";
        } catch (ServletException e) {
            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_ERROR,
                    FacesUtil.getMensagemI18n("erro_credenciais_nao_confere"));
            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO,
                    FacesUtil.getMensagemI18n("info_aluno"));
            log.debug("Erro ao logar usuário!");
            log.error(e.getMessage());
            if (e.getCause() != null) {
                log.error(e.getCause().getMessage());
            }
            return null;
        }
    }

    public String logout() throws ServletException {
        FacesUtil.getRequest().setAttribute(ControleDeSeguranca.USUARIO_LOGADO, null);
        FacesUtil.getRequest().logout();
        if (!conversation.isTransient()) {
            conversation.end();
            log.debug("Finalizando Controller de Segurança");
        }
        return "logout";
    }

    public void redefineSenhaUsuario() throws IOException {
        if (seguranca.isPrimeiroAcesso()) {
            RequestContext.getCurrentInstance().execute("PF('dlgRedefineSenha').show();");
        }
    }

    public void salvaNovaSenha() throws IOException {
        Usuario u = getUsuarioLogado();
        if (u.getSenha().equals(u.getSenhaConfirmacao())) {
            u.setSenha(CriptografiaSHA.hash256(u.getSenha()));
            seguranca.redefineSenhaUser(u);
        }
    }

    public Usuario getUsuarioLogado() {
        Usuario user = seguranca.getUsuarioLogado();
        if (!conversation.isTransient()) {
            conversation.end();
            log.debug("Finalizando Controller de Segurança");
        }
        return user;
    }

    public void reenviaSenhaUsuario() {
        if (seguranca.enviarSenhaUserPorEmail(this.email)) {
            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO,
                    "Senha redefinida e enviada para seu email com sucesso!");
        } else {
            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_WARN, "Oops! Ocorreu um erro ao redefinir a senha! Verifique o email informado e tente novamente!");
        }
    }

    public void onExpireSession() {
        RequestContext.getCurrentInstance().execute("location.href='/login.xhtml'");
        FacesContext.getCurrentInstance().addMessage("globalMessages", new FacesMessage(FacesMessage.SEVERITY_WARN,
                "Seção Expirou!", "Sua seção expirou! Faça login novamente!"));
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
