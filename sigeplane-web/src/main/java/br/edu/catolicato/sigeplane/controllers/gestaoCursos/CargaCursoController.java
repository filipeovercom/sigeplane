package br.edu.catolicato.sigeplane.controllers.gestaoCursos;

import br.edu.catolicato.sigeplane.domain.Curso;
import br.edu.catolicato.sigeplane.importacao.ImportCurso;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 07 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Named
@ViewScoped
public class CargaCursoController implements Serializable {
    private static final long serialVersionUID = 1L;

    @EJB
    private Repositorio repositorio;
    @Inject
    private ImportCurso importCurso;
    private List<Curso> cursos;
    private UploadedFile uploadedFile;

    @PostConstruct
    public void init() {
        this.importCurso.setRepositorio(repositorio);
        cursos = new LinkedList<>();
    }

    public List<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public void fileUploadListener(FileUploadEvent event) {
        uploadedFile = event.getFile();
        lerArquivo();
        RequestContext.getCurrentInstance().update("tabelaCursos");
    }

    public void lerArquivo() {
        if (uploadedFile != null) {
            cursos = importCurso.lerArquivo(this.uploadedFile);
            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Foram lidos " + cursos.size() + " registro(s)");
        }
    }

    public void salvarDados() {
        if (cursos.size() > 0) {
            boolean ret = importCurso.salvarDados();
            if (ret) {
                cursos = new LinkedList<>();
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Registros salvos com sucesso!");
            }
        } else {
            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Nenhum registro importado!");
        }
    }

}
