package br.edu.catolicato.sigeplane.importacao;

import br.edu.catolicato.sigeplane.domain.CalendarioAcademico;
import br.edu.catolicato.sigeplane.domain.CalendarioDisciplina;
import br.edu.catolicato.sigeplane.domain.Disciplina;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioDisciplinaService;
import br.edu.catolicato.sigeplane.servicos.gestaoDisciplina.DisciplinaService;
import br.edu.catolicato.sigeplane.servicos.util.ServicosUteis;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import com.csvreader.CsvReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;

/**
 * Criado por Filipe D. Abreu em 06 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class ImportCalendDisciplina implements Serializable {
    private static final long serialVersionUID = 1L;
    private Logger log;

    @Inject
    private CalendarioDisciplina horarioDisciplina;
    @Inject
    private DisciplinaService disciplinaService;
    @Inject
    private CalendarioDisciplinaService calendarioDisciplinaService;

    @PostConstruct
    public void init() {
        log = LogManager.getLogger(ImportCalendDisciplina.class.getName());
    }

    public void setRepositorio(Repositorio repositorio) {
        this.disciplinaService.setRepositorio(repositorio);
        this.calendarioDisciplinaService.setRepositorio(repositorio);
    }

    public boolean lerArquivo(UploadedFile file, CalendarioAcademico calendarioAcademico) {
        try {
            CsvReader csvReader = new CsvReader(file.getInputstream(), Charset.forName("UTF-8"));
            csvReader.setDelimiter(';');
            csvReader.readHeaders();
            int linha = 2;
            while (csvReader.readRecord()) {
                String diaSemana = csvReader.get("dia_semana");
                String codigoDisciplina = csvReader.get("codigo_disciplina");
                log.info("Lendo linha " + linha + " do arquivo!");
                if (!diaSemana.isEmpty() && !codigoDisciplina.isEmpty()) {
                    Disciplina disciplina = disciplinaService.findDisciplinaByCodigo(codigoDisciplina);
                    if (disciplina != null) {
                        if (calendarioDisciplinaService.findHrByDisciCalend(disciplina, calendarioAcademico) == null) {
                            CalendarioDisciplina calendarioDisciplina = new CalendarioDisciplina(disciplina, calendarioAcademico, ServicosUteis.getDiaSemanaByString(diaSemana));
                            calendarioAcademico.addCalendDisciplina(calendarioDisciplina);
                            calendarioDisciplina.getDisciplina().getHorarios().add(calendarioDisciplina);
                        } else {
                            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_WARN, "Erro ao ler a linha " + linha + ", já existe horário para a disciplina e calendário informados!");
                            return false;
                        }
                    } else {
                        FacesUtil.mostraMensagem(FacesMessage.SEVERITY_WARN, "Erro ao ler a linha " + linha + ", a disciplina informada não existe na base de dados!");
                        return false;
                    }
                } else {
                    FacesUtil.mostraMensagem(FacesMessage.SEVERITY_WARN, "Erro ao ler a linha " + linha + ", verifique os dados informados!");
                }
                linha++;
            }
            return true;
        } catch (IOException ex) {
            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_FATAL, "Erro ao ler o arquivo! Verifique se o mesmo está de acordo com o layout estabelecido pelo sistema!");
            log.error("Erro ao importar registros do arquivo: " + file.getFileName());
            log.error(ex.getMessage());
            return false;
        }
    }
}
