package br.edu.catolicato.sigeplane.controllers.gestaoCursos;

import br.edu.catolicato.sigeplane.domain.Curso;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.gestaoCurso.CursoService;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 07 de Julho de 2015. SIGEPLANE - Sistema de
 * Gestão de Plano de Ensino Faculdade Católica do Tocantins - Sistemas de
 * Informação
 */
@Named
@ViewScoped
public class AlteracoesCursosController implements Serializable {

    private static final long serialVersionUID = 1L;

    @EJB
    private Repositorio repositorio;
    @Inject
    private CursoService cursoService;
    private List<Curso> lista;
    private List<Curso> listaFiltro;
    @Inject
    private Curso novoCurso;
    private Curso cursoSelecionado;

    @PostConstruct
    public void init() {
        this.cursoService.setRepositorio(repositorio);
    }

    @Transactional
    public void salvarDados() {
        if (novoCurso != null) {
            boolean ret = cursoService.salvar(novoCurso);
            if (ret) {
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Sucesso!", "Registro salvo com sucesso!");
                novoCurso = new Curso();
                lista = null;
                RequestContext.getCurrentInstance().execute("PF('dlgAdicionar').hide()");
                RequestContext.getCurrentInstance().update("tabelaCursos");
            }
        }
    }

    @Transactional
    public void editarDados() {
        if (cursoSelecionado != null) {
            boolean ret = cursoService.editar(cursoSelecionado);
            if (ret) {
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Sucesso!", "Registro editado com sucesso!");
                cursoSelecionado = null;
                lista = null;
                RequestContext.getCurrentInstance().execute("PF('dlgEditar').hide()");
                RequestContext.getCurrentInstance().update("tabelaCursos");
                RequestContext.getCurrentInstance().update("panelMenu");
            }
        }
    }

    @Transactional
    public void excluirCurso() {
        if (cursoSelecionado != null) {
            boolean ret = cursoService.excluir(cursoSelecionado);
            if (ret) {
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Sucesso!", "Registro excluído com sucesso!");
                cursoSelecionado = null;
                lista = null;
                RequestContext.getCurrentInstance().update("tabelaCursos");
                RequestContext.getCurrentInstance().update("panelMenu");
            }
        }
    }

    public List<Curso> getLista() {
        if (lista == null) {
            lista = cursoService.buscaTodos();
        }
        return lista;
    }

    public void setLista(List<Curso> lista) {
        this.lista = lista;
    }

    public Curso getNovoCurso() {
        return novoCurso;
    }

    public void setNovoCurso(Curso novoCurso) {
        this.novoCurso = novoCurso;
    }

    public Curso getCursoSelecionado() {
        return cursoSelecionado;
    }

    public void setCursoSelecionado(Curso cursoSelecionado) {
        this.cursoSelecionado = cursoSelecionado;
    }

    public List<Curso> getListaFiltro() {
        return listaFiltro;
    }

    public void setListaFiltro(List<Curso> listaFiltro) {
        this.listaFiltro = listaFiltro;
    }
}
