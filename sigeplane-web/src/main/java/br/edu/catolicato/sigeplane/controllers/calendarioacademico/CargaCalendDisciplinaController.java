package br.edu.catolicato.sigeplane.controllers.calendarioacademico;

import br.edu.catolicato.sigeplane.domain.CalendarioAcademico;
import br.edu.catolicato.sigeplane.importacao.ImportCalendDisciplina;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioAcademicoService;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 25 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Named
@ViewScoped
public class CargaCalendDisciplinaController implements Serializable {
    private static final long serialVersionUID = 1L;

    @EJB
    private Repositorio repositorio;
    @Inject
    private ImportCalendDisciplina importCalendDisciplina;
    @Inject
    private CalendarioAcademicoService calendarioAcademicoService;
    private UploadedFile uploadedFile;
    private CalendarioAcademico calendSelecionado;
    private List<CalendarioAcademico> listaCalendarios;

    @PostConstruct
    public void init() {
        this.calendarioAcademicoService.setRepositorio(repositorio);
        this.importCalendDisciplina.setRepositorio(repositorio);
    }

    public void fileUploadListener(FileUploadEvent event) throws IOException {
        uploadedFile = event.getFile();
        lerArquivo();
    }

    public void lerArquivo() throws IOException {
        if (calendSelecionado != null && uploadedFile != null) {
            boolean result = importCalendDisciplina.lerArquivo(uploadedFile, calendSelecionado);
            if (result && calendSelecionado.getDisciplinas().size() > 0) {
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Sucesso!", "Foram lidos " +
                        calendSelecionado.getDisciplinas().size() + " registro(s)");
                RequestContext.getCurrentInstance().update("tabelaHorarios");
            }
        } else {
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Ooops!",
                    "Calendário Acadêmico não informado ou aconteceu algum erro ao importar seu arquivo!");
        }
    }

    public void salvarDados() {
        if (calendSelecionado != null && calendSelecionado.getDisciplinas().size() > 0) {
            boolean ret = calendarioAcademicoService.editarCalendario(calendSelecionado);
            if (ret) {
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Sucesso!", "Registros salvos com sucesso!");
                calendSelecionado = null;
                RequestContext context = RequestContext.getCurrentInstance();
                context.update("inputCalend");
                context.update("tabelaHorarios");
            }
        } else {
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Ooops!",
                    "Calendário Acadêmico não informado ou aconteceu algum erro ao importar seu arquivo!");
        }
    }

    public void selectCalendListener() {
        if (calendSelecionado.getDisciplinas().size() > 0) {
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Oops!", "Calendário " +
                    calendSelecionado.getReferencia() + " já possui horário de diciplinas!");
            calendSelecionado = null;
            RequestContext.getCurrentInstance().update("inputCalend");
        }
    }

    public CalendarioAcademico getCalendSelecionado() {
        return calendSelecionado;
    }

    public void setCalendSelecionado(CalendarioAcademico calendSelecionado) {
        this.calendSelecionado = calendSelecionado;
    }

    public List<CalendarioAcademico> getListaCalendarios() {
        if (listaCalendarios == null) {
            listaCalendarios = calendarioAcademicoService.buscaTodos();
        }
        return listaCalendarios;
    }

    public void setListaCalendarios(List<CalendarioAcademico> listaCalendarios) {
        this.listaCalendarios = listaCalendarios;
    }
}
