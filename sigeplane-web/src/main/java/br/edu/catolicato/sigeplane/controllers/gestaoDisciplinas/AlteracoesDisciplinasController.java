package br.edu.catolicato.sigeplane.controllers.gestaoDisciplinas;

import br.edu.catolicato.sigeplane.domain.Curso;
import br.edu.catolicato.sigeplane.domain.Disciplina;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.gestaoCurso.CursoService;
import br.edu.catolicato.sigeplane.servicos.gestaoDisciplina.DisciplinaService;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 17 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Named
@ViewScoped
public class AlteracoesDisciplinasController implements Serializable {
    private static final long serialVersionUID = 1L;

    @EJB
    private Repositorio repositorio;
    @Inject
    private DisciplinaService disciplinaService;
    @Inject
    private CursoService cursoService;
    private List<Disciplina> disciplinas;
    private List<Disciplina> disciplinasFiltro;
    private List<Curso> cursos;
    @Inject
    private Disciplina novaDisciplina;
    private Disciplina disciplinaSelecionada;

    @PostConstruct
    public void init() {
        this.disciplinaService.setRepositorio(repositorio);
        this.cursoService.setRepositorio(repositorio);
    }

    public void salvarDados() throws IllegalAccessException, InstantiationException {
        if (novaDisciplina != null) {
            boolean ret = disciplinaService.salvar(novaDisciplina);
            if (ret) {
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Registro salvo com sucesso!");
                novaDisciplina = Disciplina.class.newInstance();
                disciplinas = null;
                RequestContext.getCurrentInstance().execute("PF('dlgAdicionar').hide()");
                RequestContext.getCurrentInstance().update("tabelaDisciplinas");
            } else {
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Erro ao salvar registro!");
            }
        }
    }

    public void editarDados() throws IllegalAccessException, InstantiationException {
        if (disciplinaSelecionada != null) {
            boolean ret = disciplinaService.editar(disciplinaSelecionada);
            if (ret) {
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Registro editado com sucesso!");
                disciplinaSelecionada = null;
                disciplinas = null;
                RequestContext.getCurrentInstance().execute("PF('dlgEditar').hide()");
                RequestContext.getCurrentInstance().update("tabelaDisciplinas");
                RequestContext.getCurrentInstance().update("panelMenu");
            } else {
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Erro ao editar registro!");
            }
        }
    }

    public void excluirDisciplina() {
        if (disciplinaSelecionada != null) {
            boolean ret = disciplinaService.excluir(disciplinaSelecionada);
            if (ret) {
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Registro excluído com sucesso!");
                disciplinaSelecionada = null;
                disciplinas = null;
                RequestContext.getCurrentInstance().update("tabelaDisciplinas");
                RequestContext.getCurrentInstance().update("panelMenu");
            } else {
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Erro ao excluir registro!");
            }
        }
    }

    public List<Disciplina> getDisciplinas() {
        if (disciplinas == null) {
            this.disciplinas = this.disciplinaService.buscaTodos();
        }
        return disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public List<Disciplina> getDisciplinasFiltro() {
        return disciplinasFiltro;
    }

    public void setDisciplinasFiltro(List<Disciplina> disciplinasFiltro) {
        this.disciplinasFiltro = disciplinasFiltro;
    }

    public List<Curso> getCursos() {
        if (cursos == null) {
            this.cursos = this.cursoService.buscaTodos();
        }
        return cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public Disciplina getNovaDisciplina() {
        return novaDisciplina;
    }

    public void setNovaDisciplina(Disciplina novaDisciplina) {
        this.novaDisciplina = novaDisciplina;
    }

    public Disciplina getDisciplinaSelecionada() {
        return disciplinaSelecionada;
    }

    public void setDisciplinaSelecionada(Disciplina disciplinaSelecionada) {
        this.disciplinaSelecionada = disciplinaSelecionada;
    }
}
