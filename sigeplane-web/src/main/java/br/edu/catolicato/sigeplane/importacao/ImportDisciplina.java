package br.edu.catolicato.sigeplane.importacao;

import br.edu.catolicato.sigeplane.domain.Curso;
import br.edu.catolicato.sigeplane.domain.Disciplina;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.LeituraCSV;
import br.edu.catolicato.sigeplane.servicos.gestaoCurso.CursoService;
import br.edu.catolicato.sigeplane.servicos.gestaoDisciplina.DisciplinaService;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import com.csvreader.CsvReader;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 06 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class ImportDisciplina implements LeituraCSV<Disciplina>, Serializable {
    private static final long serialVersionUID = 1L;
    private Logger log;
    private List<Disciplina> disciplinasSalvar;
    private List<Disciplina> disciplinasEditar;

    @Inject
    private DisciplinaService disciplinaService;
    @Inject
    private CursoService cursoService;
    private Repositorio repositorio;

    @PostConstruct
    public void init() {
        log = LogManager.getLogger(ImportDisciplina.class.getName());
    }

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
        this.disciplinaService.setRepositorio(repositorio);
        this.cursoService.setRepositorio(repositorio);
    }

    @Override
    public List<Disciplina> lerArquivo(UploadedFile file) {
        try {
            disciplinasSalvar = new LinkedList<>();
            disciplinasEditar = new LinkedList<>();

            CsvReader csvReader = new CsvReader(file.getInputstream(), Charset.defaultCharset());
            csvReader.setDelimiter(';');
            csvReader.readHeaders();
            Curso cursoAux = null;
            int linha = 1;
            while (csvReader.readRecord()) {
                String codDisciplina = csvReader.get("codigo");
                String nome = csvReader.get("nome");
                String curso = csvReader.get("curso");
                String ementa = csvReader.get("ementa");
                String turma = csvReader.get("turma");
                Integer cargaHTotal = 0;
                Integer cargaHPratica = 0;
                Integer cargaHTeorica = 0;
                if (!csvReader.get("chtotal").isEmpty()) {
                    cargaHTotal = Integer.valueOf(csvReader.get("chtotal"));
                }
                if (!csvReader.get("chpratica").isEmpty()) {
                    cargaHPratica = Integer.valueOf(csvReader.get("chpratica"));
                }
                if (!csvReader.get("chteorica").isEmpty()) {
                    cargaHTeorica = Integer.valueOf(csvReader.get("chteorica"));
                }
                log.info("Lendo linha " + linha);
                if (!codDisciplina.isEmpty() && !nome.isEmpty()) {
                    Disciplina aux = disciplinaService.findDisciplinaByCodigo(codDisciplina);
                    if (cursoAux == null) {
                        cursoAux = cursoService.findCursoByNome(curso);
                    } else if (!cursoAux.getNome().equalsIgnoreCase(curso)) {
                        cursoAux = cursoService.findCursoByNome(curso);
                    }
                    if (aux != null) {
                        aux.setCodigo(codDisciplina);
                        aux.setNome(nome);
                        aux.setEmenta(ementa);
                        aux.setTurma(turma);
                        cursoAux.addDisciplina(aux);
                        aux.setCargaHorariaPratica(cargaHPratica);
                        aux.setCargaHorariaTeorica(cargaHTeorica);
                        aux.setCargaHorariaTotal(cargaHTotal);
                        disciplinasEditar.add(aux);
                    } else {
                        aux = new Disciplina(codDisciplina, nome, ementa, cursoAux, cargaHTeorica,
                                cargaHPratica, cargaHTotal, turma);
                        if(!disciplinasSalvar.contains(aux)) {
                            disciplinasSalvar.add(aux);
                        } else {
                            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Oops!", "Erro ao ler a linha " + linha + ", registro duplicado!");
                        }
                    }
                } else {
                    FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Oops!", "Erro ao ler a linha " + linha + ", verifique os dados informados!");
                }
                linha++;
            }
            List<Disciplina> retorno = new LinkedList<>();
            retorno.addAll(disciplinasEditar);
            retorno.addAll(disciplinasSalvar);
            return retorno;
        } catch (IOException ex) {
            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_FATAL, "Erro ao ler o arquivo! Verifique se o mesmo está de acordo com o layout estabelecido pelo sistema!");
            log.error("Erro ao importar registros do arquivo: " + file.getFileName());
            log.error(ex.getMessage(), Level.ERROR);
            return null;
        }
    }

    @Override
    public boolean salvarDados() {
        boolean ret = true;
        if (disciplinasSalvar != null && disciplinasSalvar.size() > 0) {
            if (!disciplinaService.salvarDisciplinas(disciplinasSalvar)) {
                ret = false;
            }
        }
        if (ret) {
            if (disciplinasEditar != null && disciplinasEditar.size() > 0) {
                if (!disciplinaService.editarDisciplinas(disciplinasEditar)) {
                    ret = false;
                }
            }
        }
        return ret;
    }
}
