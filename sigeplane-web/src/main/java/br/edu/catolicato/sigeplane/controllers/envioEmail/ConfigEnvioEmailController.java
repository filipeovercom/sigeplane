package br.edu.catolicato.sigeplane.controllers.envioEmail;

import br.edu.catolicato.sigeplane.domain.ConfigEnvioEmail;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.envioEmail.ConfigEnvioEmailService;
import br.edu.catolicato.sigeplane.util.FacesUtil;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Criado por Filipe D. Abreu em 22 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Named
@ViewScoped
public class ConfigEnvioEmailController implements Serializable {
    private static final long serialVersionUID = 1L;

    private ConfigEnvioEmail configEnvioEmail;
    @EJB
    private Repositorio repositorio;
    @Inject
    private ConfigEnvioEmailService configEnvioEmailService;

    @PostConstruct
    private void init() {
        this.configEnvioEmailService.setRepositorio(repositorio);
        this.configEnvioEmail = this.configEnvioEmailService.getConfiguracao();
    }

    public void salvarConfiguracao(){
        boolean result = this.configEnvioEmailService.editarConfiguracao(this.configEnvioEmail);
        if(result){
            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Configuração salva com sucesso!");
        } else {
            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_ERROR, "Erro ao salvar configuração! Tente Novamente");
        }
    }

    public ConfigEnvioEmail getConfigEnvioEmail() {
        return configEnvioEmail;
    }

    public void setConfigEnvioEmail(ConfigEnvioEmail configEnvioEmail) {
        this.configEnvioEmail = configEnvioEmail;
    }
}
