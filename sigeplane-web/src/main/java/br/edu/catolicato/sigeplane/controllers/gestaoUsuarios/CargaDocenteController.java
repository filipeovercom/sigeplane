package br.edu.catolicato.sigeplane.controllers.gestaoUsuarios;

import br.edu.catolicato.sigeplane.domain.Docente;
import br.edu.catolicato.sigeplane.importacao.ImportProfessor;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 07 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Named
@ViewScoped
public class CargaDocenteController implements Serializable {
    private static final long serialVersionUID = 1L;

    @EJB
    private Repositorio repositorio;
    @Inject
    private ImportProfessor ImportProfessor;
    private List<Docente> docentes;
    private UploadedFile uploadedFile;

    @PostConstruct
    public void init() {
        this.ImportProfessor.setRepositorio(repositorio);
        docentes = new LinkedList<>();
    }

    public List<Docente> getDocentes() {
        return docentes;
    }

    public void setDocentes(List<Docente> docentes) {
        this.docentes = docentes;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public void fileUploadListener(FileUploadEvent event) {
        uploadedFile = event.getFile();
        lerArquivo();
        RequestContext.getCurrentInstance().update("tabelaDocentes");
    }

    public void lerArquivo() {
        if (uploadedFile != null) {
            docentes = ImportProfessor.lerArquivo(this.uploadedFile);
            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Foram lidos " + docentes.size() + " registro(s)");
        }
    }

    public void salvarDados() {
        if (docentes.size() > 0) {
            boolean ret = ImportProfessor.salvarDados();
            if (ret) {
                docentes = new LinkedList<>();
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Registros salvos com sucesso!");
            }
        }
    }

}
