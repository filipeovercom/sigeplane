package br.edu.catolicato.sigeplane.util;

import br.edu.catolicato.sigeplane.domain.StatusCalendario;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

import static javax.faces.application.FacesMessage.Severity;

/**
 * Criado por Filipe D. Abreu em 27 de Junho de 2015. SIGEPLANE - Sistema de
 * Gestão de Plano de Ensino Faculdade Católica do Tocantins - Sistemas de
 * Informação
 */
@Named
public class FacesUtil implements Serializable {

    private static final long serialVersionUID = 1L;

    public static String getMensagemI18n(String chave) {
        FacesContext context = FacesContext.getCurrentInstance();
        String msg = context.getApplication().getResourceBundle(context, "msgs").getString(chave);
        return msg;
    }

    public static void mostraMensagem(Severity tipo, String msg) {
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(tipo, msg, msg));
    }

    public static void mostraMensagem(String nameComponent, Severity tipo, String title, String message) {
        FacesContext.getCurrentInstance().addMessage(nameComponent,
                new FacesMessage(tipo, title, message));
    }

    public static Object getRequestAttribute(String nome) {
        HttpServletRequest request = getRequest();
        return request.getAttribute(nome);
    }

    public static HttpServletRequest getRequest() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        return request;
    }

    public StatusCalendario getStatusCalendario(int ordinalNumber) {
        StatusCalendario result = null;
        switch (ordinalNumber) {
            case 1:
                result = StatusCalendario.INICIADO;
                break;
            case 2:
                result = StatusCalendario.EM_ANDAMENTO;
                break;
            case 3:
                result = StatusCalendario.FECHADO;
                break;
        }
        return result;
    }
}
