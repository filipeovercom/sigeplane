package br.edu.catolicato.sigeplane.controllers.calendarioacademico;

import br.edu.catolicato.sigeplane.domain.AulaCalendario;
import br.edu.catolicato.sigeplane.importacao.ImportCalendarioAcademico;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 25 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Named
@ViewScoped
public class CargaCalendarioController implements Serializable {
    private static final long serialVersionUID = 1L;

    @EJB
    private Repositorio repositorio;
    @Inject
    private ImportCalendarioAcademico ImportCalendarioAcademico;
    private List<AulaCalendario> aulas;
    private Integer anoSemestre;
    private Integer periodoSemestre;
    private UploadedFile uploadedFile;

    @PostConstruct
    public void init() {
        ImportCalendarioAcademico.setRepositorio(repositorio);
        aulas = new LinkedList<>();
    }

    public List<AulaCalendario> getAulas() {
        return aulas;
    }

    public void setAulas(List<AulaCalendario> aulas) {
        this.aulas = aulas;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public void fileUploadListener(FileUploadEvent event) {
        uploadedFile = event.getFile();
        lerArquivo();
        RequestContext.getCurrentInstance().update("tabelaAulas");
    }

    public void lerArquivo() {
        if (uploadedFile != null) {
            aulas = ImportCalendarioAcademico.lerArquivo(this.uploadedFile);
            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Foram lidos " + aulas.size() + " registro(s)");
        }
    }

    @Transactional
    public void salvarDados() {
        if (anoSemestre != null && periodoSemestre != null && aulas.size() > 0) {
            String referencia = anoSemestre.toString() + "/" + periodoSemestre.toString();
            boolean ret = ImportCalendarioAcademico.salvarDados(referencia);
            if (ret) {
                aulas = new LinkedList<>();
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Registros salvos com sucesso!");
            } else {
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_ERROR, "Erro!", "Erro ao salvar os registros! Tente novamente!");
            }
        } else {
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Oops!", "Calendário não informado ou não foi encontrado dados no arquivo!");
        }
    }

    public Integer getAnoSemestre() {
        return anoSemestre;
    }

    public void setAnoSemestre(Integer anoSemestre) {
        this.anoSemestre = anoSemestre;
    }

    public Integer getPeriodoSemestre() {
        return periodoSemestre;
    }

    public void setPeriodoSemestre(Integer periodoSemestre) {
        this.periodoSemestre = periodoSemestre;
    }
}
