package br.edu.catolicato.sigeplane.controllers.calendarioacademico;

import br.edu.catolicato.sigeplane.domain.CalendarioAcademico;
import br.edu.catolicato.sigeplane.domain.CalendarioDisciplina;
import br.edu.catolicato.sigeplane.domain.DiaSemana;
import br.edu.catolicato.sigeplane.domain.Disciplina;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioAcademicoService;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioDisciplinaService;
import br.edu.catolicato.sigeplane.servicos.gestaoDisciplina.DisciplinaService;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.PostActivate;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 24 de Julho de 2015. SIGEPLANE - Sistema de
 * Gestão de Plano de Ensino Faculdade Católica do Tocantins - Sistemas de
 * Informação
 */
@Named
@ViewScoped
public class AlteracoesCalendDiscController implements Serializable {

    private static final long serialVersionUID = 1L;

    //-------------Serviços e Repositorio---------------------------------------------
    @EJB
    private Repositorio repositorio;
    @Inject
    private CalendarioDisciplinaService calendarioDisciplinaService;
    @Inject
    private DisciplinaService disciplinaService;
    @Inject
    private CalendarioAcademicoService calendService;
    //--------------------------------------------------------------------------------
    //-------------- Atributos -------------------------------------------------------
    private CalendarioAcademico calendRowSelecionado;
    private CalendarioAcademico calendarioSelecionado;
    @Inject
    private CalendarioDisciplina horarioDisciplinaAux;
    private List<CalendarioAcademico> listaCalendComHorarios;
    private List<CalendarioAcademico> listaHorariosFiltrados;
    private List<CalendarioAcademico> listaCalendarios;
    private List<CalendarioDisciplina> horarioDisciplinas;
    private List<Disciplina> disciplinas;
    private DiaSemana[] diaSemanas;
    //-------------------------------------------------------------------------------

    @PostConstruct
    @PostActivate
    public void init() {
        this.calendService.setRepositorio(repositorio);
        this.calendarioDisciplinaService.setRepositorio(repositorio);
        this.disciplinaService.setRepositorio(repositorio);
    }


    public void salvaHorario() {
        if (calendarioSelecionado != null) {
            if (horarioDisciplinas != null
                    && horarioDisciplinas.size() > 0) {
                if (calendarioSelecionado.getDisciplinas().size() == 0) {
                    for (CalendarioDisciplina a : horarioDisciplinas) {
                        calendarioSelecionado.addCalendDisciplina(a);
                        a.getDisciplina().getHorarios().add(a);
                    }
                    if (calendService.editarCalendario(calendarioSelecionado)) {
                        calendarioSelecionado = new CalendarioAcademico();
                        listaCalendComHorarios = null;
                        horarioDisciplinas = null;
                        horarioDisciplinaAux = new CalendarioDisciplina();
                        disciplinas = null;
                        RequestContext context = RequestContext.getCurrentInstance();
                        context.execute("ocultaFormAdicionar()");
                        context.update("tabelaCalendarios");
                        context.update("conteudoFormAdd");
                        FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Sucesso!", "Dados salvos com sucesso!");
                    } else {
                        FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_ERROR, "Erro!", "Erro ao salvar Registro! Verifique os dados e Tente novamente!");
                    }
                } else {
                    FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Oops!", "Já existe horários para o calendário informado!");
                }
            } else {
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Oops!", "Nenhuma disciplina foi informada!");
            }
        } else {
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Oops!", "Calendário não informado!");
        }
    }


    public void editaHorario() {
        if (calendService.editarCalendario(calendRowSelecionado)) {
            calendRowSelecionado = null;
            listaCalendComHorarios = null;
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("ocultaFormEditar();");
            context.update("tabelaCalendarios");
            context.update("panelMenu");
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Sucesso!", "Registro editado com sucesso!");
        } else {
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Erro!", "Erro ao editar Registro! Atualize a página e Tente novamente!");
        }
    }


    public void excluiHorario() {
        if (calendRowSelecionado != null) {
            calendRowSelecionado.getDisciplinas().clear();
            if (calendService.editarCalendario(calendRowSelecionado)) {
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Sucesso!", "Registro excluido com sucesso!");
                calendRowSelecionado = null;
                listaCalendComHorarios = null;
                RequestContext.getCurrentInstance().update("tabelaCalendarios");
            } else {
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Erro!", "Erro ao editar Registro! Atualize a página e Tente novamente!");
            }
        }
    }

    public void adicionaHorarioCalendario() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        if (calendRowSelecionado == null) {
            if (horarioDisciplinaAux.getDiaSemana() != null && horarioDisciplinaAux.getDisciplina() != null) {
                getHorarioDisciplinas().add(horarioDisciplinaAux);
                disciplinas.remove(horarioDisciplinaAux.getDisciplina());
                requestContext.update("tabelaDisciplinas");
                requestContext.update("tabelaHrAdd");
            } else {
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Oops!", "Dia da semana e Disciplina devem ser informados informado!");
            }
        } else {
            if (horarioDisciplinaAux.getDiaSemana() != null && horarioDisciplinaAux.getDisciplina() != null) {
                calendRowSelecionado.addCalendDisciplina(horarioDisciplinaAux);
                disciplinas.remove(horarioDisciplinaAux.getDisciplina());
                requestContext.update("tabelaDisciplinasEditar");
                requestContext.update("tabelaHrEdit");
            } else {
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Oops!", "Dia da semana e Disciplina devem ser informados informado!");
            }
        }
        horarioDisciplinaAux = new CalendarioDisciplina();
    }

    public void removeHorarioCalendario(CalendarioDisciplina pHorarioDisciplina) {
        if (calendRowSelecionado == null) {
            horarioDisciplinas.remove(pHorarioDisciplina);
            disciplinas.add(pHorarioDisciplina.getDisciplina());
        } else {
            calendRowSelecionado.getDisciplinas().remove(pHorarioDisciplina);
            disciplinas.add(pHorarioDisciplina.getDisciplina());
        }
    }

    public void inicializaFormAdicionar() {
        disciplinas = disciplinaService.buscaTodos();
    }

    public void inicializaFormEditar() {
        disciplinas = disciplinaService.buscaTodos();
        for (CalendarioDisciplina d : calendRowSelecionado.getDisciplinas()){
            if(disciplinas.contains(d.getDisciplina())) {
                disciplinas.remove(d.getDisciplina());
            }
        }
    }

    public void limparFormAdicionar() {
        horarioDisciplinaAux = new CalendarioDisciplina();
        horarioDisciplinas = new LinkedList<>();
        calendarioSelecionado = new CalendarioAcademico();
        disciplinas = null;
    }

    public void limpaFormEditar() {
        horarioDisciplinaAux = new CalendarioDisciplina();
        calendRowSelecionado = null;
        disciplinas = null;
    }

    public CalendarioAcademico getCalendRowSelecionado() {
        return calendRowSelecionado;
    }

    public void setCalendRowSelecionado(CalendarioAcademico calendRowSelecionado) {
        this.calendRowSelecionado = calendRowSelecionado;
    }

    public List<CalendarioAcademico> getListaCalendComHorarios() {
        if (listaCalendComHorarios == null) {
            listaCalendComHorarios = calendService.findAllCalendComHrDisciplinas();
        }
        return listaCalendComHorarios;
    }

    public void setListaCalendComHorarios(List<CalendarioAcademico> listaCalendComHorarios) {
        this.listaCalendComHorarios = listaCalendComHorarios;
    }

    public List<CalendarioAcademico> getListaHorariosFiltrados() {
        return listaHorariosFiltrados;
    }

    public void setListaHorariosFiltrados(List<CalendarioAcademico> listaHorariosFiltrados) {
        this.listaHorariosFiltrados = listaHorariosFiltrados;
    }

    public List<CalendarioAcademico> getListaCalendarios() {
        if (listaCalendarios == null) {
            listaCalendarios = calendService.buscaTodos();
        }
        return listaCalendarios;
    }

    public void setListaCalendarios(List<CalendarioAcademico> listaCalendarios) {
        this.listaCalendarios = listaCalendarios;
    }

    public List<Disciplina> getDisciplinas() {
        if (disciplinas == null) {
            disciplinas = new LinkedList<>();
        }
        return disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public CalendarioDisciplina getHorarioDisciplinaAux() {
        return horarioDisciplinaAux;
    }

    public void setHorarioDisciplinaAux(CalendarioDisciplina horarioDisciplinaAux) {
        this.horarioDisciplinaAux = horarioDisciplinaAux;
    }

    public DiaSemana[] getDiaSemanas() {
        if (diaSemanas == null) {
            diaSemanas = DiaSemana.values();
        }
        return diaSemanas;
    }

    public void setDiaSemanas(DiaSemana[] diaSemanas) {
        this.diaSemanas = diaSemanas;
    }

    public CalendarioAcademico getCalendarioSelecionado() {
        return calendarioSelecionado;
    }

    public void setCalendarioSelecionado(CalendarioAcademico calendarioSelecionado) {
        this.calendarioSelecionado = calendarioSelecionado;
    }

    public List<CalendarioDisciplina> getHorarioDisciplinas() {
        if (horarioDisciplinas == null) {
            horarioDisciplinas = new LinkedList<>();
        }
        return horarioDisciplinas;
    }

    public void setHorarioDisciplinas(List<CalendarioDisciplina> horarioDisciplinas) {
        this.horarioDisciplinas = horarioDisciplinas;
    }
}
