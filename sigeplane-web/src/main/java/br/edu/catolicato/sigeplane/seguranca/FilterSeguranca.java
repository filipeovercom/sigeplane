package br.edu.catolicato.sigeplane.seguranca;

import javax.faces.application.ViewExpiredException;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Criado por Filipe D. Abreu em 08 de Julho de 2015. SIGEPLANE - Sistema de
 * Gestão de Plano de Ensino Faculdade Católica do Tocantins - Sistemas de
 * Informação
 */
public class FilterSeguranca implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        try {
            if (servletRequest.getServletPath().contains("/login")
                    || servletRequest.getServletPath().contains("javax.")
                    || servletRequest.getServletPath().contains("/resources")) {

                chain.doFilter(request, response);

            } else if (servletRequest.getSession(false) == null
                    || servletRequest.getSession(false).getAttribute(ControleDeSeguranca.USUARIO_LOGADO) == null) {
                String contextPath = ((HttpServletRequest) request).getContextPath();
                ((HttpServletResponse) response).sendRedirect(contextPath + "/login.xhtml");
            } else {
                chain.doFilter(request, response);
            }
        } catch (ViewExpiredException ex) {
            String contextPath = ((HttpServletRequest) request).getContextPath();
            ((HttpServletResponse) response).sendRedirect(contextPath + "/login.xhtml");
        }
    }

    @Override
    public void destroy() {
    }
}
