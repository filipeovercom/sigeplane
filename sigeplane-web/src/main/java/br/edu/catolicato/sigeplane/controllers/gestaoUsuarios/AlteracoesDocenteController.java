package br.edu.catolicato.sigeplane.controllers.gestaoUsuarios;

import br.edu.catolicato.sigeplane.domain.Docente;
import br.edu.catolicato.sigeplane.domain.Perfil;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.gestaoUsuarios.DocenteService;
import br.edu.catolicato.sigeplane.util.CriptografiaSHA;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 07 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Named
@ViewScoped
public class AlteracoesDocenteController implements Serializable {
    private static final long serialVersionUID = 1L;

    @EJB
    private Repositorio repositorio;
    @Inject
    private DocenteService docenteService;
    private List<Docente> lista;
    private List<Docente> listaFiltro;
    @Inject
    private Docente novoDocente;
    private Docente docenteSelecionado;

    @PostConstruct
    public void init() {
        this.docenteService.setRepositorio(repositorio);
    }

    public List<Docente> getLista() {
        if (lista == null) {
            lista = docenteService.buscaTodos();
        }
        return lista;
    }

    public void setLista(List<Docente> lista) {
        this.lista = lista;
    }

    public Docente getNovoDocente() {
        return novoDocente;
    }

    public void setNovoDocente(Docente novoDocente) {
        this.novoDocente = novoDocente;
    }

    public Docente getDocenteSelecionado() {
        return docenteSelecionado;
    }

    public void setDocenteSelecionado(Docente docenteSelecionado) {
        this.docenteSelecionado = docenteSelecionado;
    }

    public List<Docente> getListaFiltro() {
        return listaFiltro;
    }

    public void setListaFiltro(List<Docente> listaFiltro) {
        this.listaFiltro = listaFiltro;
    }

    public void salvarDados() throws IllegalAccessException, InstantiationException, IOException {
        if (novoDocente != null) {
            novoDocente.setSenha(CriptografiaSHA.hash256(novoDocente.getMatricula()));
            novoDocente.setPerfil(Perfil.DOCENTE);
            boolean ret = docenteService.salvarDocente(novoDocente);
            if (ret) {
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Registro salvo com sucesso!");
                novoDocente = Docente.class.newInstance();
                lista = null;
                RequestContext.getCurrentInstance().execute("PF('dlgAdicionar').hide()");
                RequestContext.getCurrentInstance().update("tabelaDocentes");
            }
        }
    }

    public void editarDados() throws IllegalAccessException, InstantiationException {
        if (docenteSelecionado != null) {
            boolean ret = docenteService.editarDocente(docenteSelecionado);
            if (ret) {
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Registro editado com sucesso!");
                docenteSelecionado = null;
                lista = null;
                RequestContext.getCurrentInstance().execute("PF('dlgEditar').hide()");
                RequestContext.getCurrentInstance().update("tabelaDocentes");
                RequestContext.getCurrentInstance().update("panelMenu");
            }
        }
    }

    public void excluirDocente() {
        if (docenteSelecionado != null) {
            boolean ret = docenteService.excluirDocente(docenteSelecionado);
            if (ret) {
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Registro excluído com sucesso!");
                docenteSelecionado = null;
                lista = null;
                RequestContext.getCurrentInstance().update("tabelaDocentes");
                RequestContext.getCurrentInstance().update("panelMenu");
            }
        }
    }
}
