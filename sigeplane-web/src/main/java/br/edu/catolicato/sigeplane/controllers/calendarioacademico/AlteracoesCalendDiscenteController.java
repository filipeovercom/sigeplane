package br.edu.catolicato.sigeplane.controllers.calendarioacademico;

import br.edu.catolicato.sigeplane.domain.CalendarioAcademico;
import br.edu.catolicato.sigeplane.domain.CalendarioDiscente;
import br.edu.catolicato.sigeplane.domain.Discente;
import br.edu.catolicato.sigeplane.domain.Disciplina;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioAcademicoService;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioDiscenteService;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioDisciplinaService;
import br.edu.catolicato.sigeplane.servicos.gestaoDisciplina.DisciplinaService;
import br.edu.catolicato.sigeplane.servicos.gestaoUsuarios.DiscenteService;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DualListModel;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 20 de Agosto de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Named
@ViewScoped
public class AlteracoesCalendDiscenteController implements Serializable {
    private static final long serialVersionUID = 1L;

    //------------------ Serviços e Repositorio -----------------------------------
    @EJB
    private Repositorio repositorio;
    @Inject
    private CalendarioDiscenteService service;
    @Inject
    private DisciplinaService disciplinaService;
    @Inject
    private CalendarioDisciplinaService calendarioDisciplinaService;
    @Inject
    private DiscenteService discenteService;
    @Inject
    private CalendarioAcademicoService calendService;

    //------------------ Atributos -----------------------------------
    @Inject
    private CalendarioDiscente novoHorario;
    private CalendarioDiscente horarioSelecionado;
    private List<CalendarioDiscente> listaHorarios;
    private List<CalendarioDiscente> listaHorariosFiltrados;
    private List<CalendarioAcademico> listaCalendarios;
    private List<Disciplina> disciplinasDisponiveis;
    private List<Discente> discentes;
    private DualListModel<Disciplina> disciplinaDualListModel;

    @PostConstruct
    public void init() {
        this.service.setRepositorio(repositorio);
        this.discenteService.setRepositorio(repositorio);
        this.disciplinaService.setRepositorio(repositorio);
        this.calendarioDisciplinaService.setRepositorio(repositorio);
        this.calendService.setRepositorio(repositorio);
        listaHorarios = service.buscaTodos();
    }

    public void salvarHorario() {
        if (service.findHrByDiscenteCalendario(novoHorario.getDiscente(), novoHorario.getCalendarioAcademico()) == null) {
            novoHorario.getDisciplinas().addAll(disciplinaDualListModel.getTarget());
            for (Disciplina d : novoHorario.getDisciplinas()) {
                d.getHorariosDiscentes().add(novoHorario);
            }
            novoHorario.getCalendarioAcademico().getDiscentes().add(novoHorario);
            if (calendService.editarCalendario(this.novoHorario.getCalendarioAcademico())) {
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Sucesso!", "Registro salvo com sucesso!");
                novoHorario = new CalendarioDiscente();
                listaHorarios = null;
                disciplinaDualListModel = null;
                RequestContext.getCurrentInstance().update("tabelaHorarios");
                RequestContext.getCurrentInstance().update("conteudoFormAdd");
                RequestContext.getCurrentInstance().execute("fechaFormAdd();");
            } else {
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_ERROR, "Erro!", "Erro ao salvar Registro! Verifique os dados e Tente novamente!");
            }
        } else {
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Oops!", "O aluno informado já tem horário para o semestre informado!");
        }
    }

    public void editarHorario() {
        if (service.editarHorario(this.horarioSelecionado)) {
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Sucesso!", "Registro editado com sucesso!");
            horarioSelecionado = null;
            listaHorarios = null;
            RequestContext.getCurrentInstance().update("tabelaHorarios");
            RequestContext.getCurrentInstance().update("conteudoFormEdit");
            RequestContext.getCurrentInstance().execute("fechaFormEdit();");
        } else {
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Erro!", "Erro ao editar Registro! Atualize a página e Tente novamente!");
        }
    }

    public void excluirHorario() {
        if (service.excluirHorario(this.horarioSelecionado)) {
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Sucesso!", "Registro excluido com sucesso!");
            horarioSelecionado = null;
            listaHorarios = null;
            RequestContext.getCurrentInstance().update("tabelaHorarios");
        } else {
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Erro!", "Erro ao editar Registro! Atualize a página e Tente novamente!");
        }
    }

    public List<Discente> completeDiscente(String query) {
        List<Discente> filteredDiscentes = new ArrayList<>();

        if (discentes == null) {
            discentes = discenteService.buscaTodos();
        }

        for (int i = 0; i < discentes.size(); i++) {
            Discente discente = discentes.get(i);
            if (discente.getNome().toLowerCase().startsWith(query.toLowerCase())
                    || discente.getMatricula().toLowerCase().startsWith(query.toLowerCase())) {
                filteredDiscentes.add(discente);
            }
        }
        return filteredDiscentes;
    }

    public void inicializaPickListDisciplina() {
        if (novoHorario.getCalendarioAcademico() != null) {
            disciplinasDisponiveis = calendarioDisciplinaService.findDisciplinasWithCalendario(novoHorario.getCalendarioAcademico());
        }
        this.disciplinaDualListModel = new DualListModel<>(disciplinasDisponiveis, new ArrayList<Disciplina>());
        //TODO -> Retirar do source todas as disciplinas que estão agendadas para o aluno selecionado em caso de edição!
    }

    public CalendarioDiscente getNovoHorario() {
        return novoHorario;
    }

    public void setNovoHorario(CalendarioDiscente novoHorario) {
        this.novoHorario = novoHorario;
    }

    public CalendarioDiscente getHorarioSelecionado() {
        return horarioSelecionado;
    }

    public void setHorarioSelecionado(CalendarioDiscente horarioSelecionado) {
        this.horarioSelecionado = horarioSelecionado;
    }

    public List<CalendarioDiscente> getListaHorarios() {
        if (listaHorarios == null) {
            listaHorarios = service.buscaTodos();
        }
        return listaHorarios;
    }

    public void setListaHorarios(List<CalendarioDiscente> listaHorarios) {
        this.listaHorarios = listaHorarios;
    }

    public List<CalendarioDiscente> getListaHorariosFiltrados() {
        return listaHorariosFiltrados;
    }

    public void setListaHorariosFiltrados(List<CalendarioDiscente> listaHorariosFiltrados) {
        this.listaHorariosFiltrados = listaHorariosFiltrados;
    }

    public List<CalendarioAcademico> getListaCalendarios() {
        if (listaCalendarios == null) {
            listaCalendarios = calendService.buscaTodos();
        }
        return listaCalendarios;
    }

    public void setListaCalendarios(List<CalendarioAcademico> listaCalendarios) {
        this.listaCalendarios = listaCalendarios;
    }

    public DualListModel<Disciplina> getDisciplinaDualListModel() {
        if (disciplinaDualListModel == null) {
            disciplinaDualListModel = new DualListModel<>(new ArrayList(), new ArrayList());
        }
        return disciplinaDualListModel;
    }

    public void setDisciplinaDualListModel(DualListModel<Disciplina> disciplinaDualListModel) {
        this.disciplinaDualListModel = disciplinaDualListModel;
    }

    public List<Disciplina> getDisciplinasDisponiveis() {
        return disciplinasDisponiveis;
    }

    public void setDisciplinasDisponiveis(List<Disciplina> disciplinasDisponiveis) {
        this.disciplinasDisponiveis = disciplinasDisponiveis;
    }

    public List<Discente> getDiscentes() {
        return discentes;
    }

    public void setDiscentes(List<Discente> discentes) {
        this.discentes = discentes;
    }
}
