package br.edu.catolicato.sigeplane.controllers.arquivos;

import br.edu.catolicato.sigeplane.domain.Arquivo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import java.io.*;

/**
 * Criado por Filipe D. Abreu em 16 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@ManagedBean
@RequestScoped
public class ArquivosController implements Serializable {
    private static final long serialVersionUID = 1L;

    private StreamedContent imagem;
    private StreamedContent pdf;
    private StreamedContent video;
    private Logger log;

    @PostConstruct
    public void init() {
        this.log = LogManager.getLogger(ArquivosController.class);
    }

    public StreamedContent getImagem(Arquivo arquivo) {
        File foto = new File(arquivo.getDiretorio() + arquivo.getNomeArquivo());
        DefaultStreamedContent content = null;
        try {
            BufferedInputStream in = new BufferedInputStream(new FileInputStream(foto));
            byte[] bytes = new byte[in.available()];
            in.read(bytes);
            in.close();
            content = new DefaultStreamedContent(new ByteArrayInputStream(bytes), "image/jpeg");
        } catch (Exception e) {
            log.error(e);
        }
        return content;
    }

    public StreamedContent getImagemFromUploadedFile(UploadedFile file) {
        DefaultStreamedContent content = null;
        try {
            BufferedInputStream in = new BufferedInputStream(file.getInputstream());
            byte[] bytes = new byte[in.available()];
            in.read(bytes);
            in.close();
            content = new DefaultStreamedContent(new ByteArrayInputStream(bytes), "image/jpeg");
        } catch (Exception e) {
            log.error(e);
        }
        return content;
    }


    public void setImagem(StreamedContent imagem) {
        this.imagem = imagem;
    }

    public StreamedContent getPdf() {
        return pdf;
    }

    public void setPdf(StreamedContent pdf) {
        this.pdf = pdf;
    }

    public StreamedContent getVideo() {
        return video;
    }

    public void setVideo(StreamedContent video) {
        this.video = video;
    }
}
