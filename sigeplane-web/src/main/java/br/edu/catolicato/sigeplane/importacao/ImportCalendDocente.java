package br.edu.catolicato.sigeplane.importacao;

import br.edu.catolicato.sigeplane.domain.CalendarioAcademico;
import br.edu.catolicato.sigeplane.domain.CalendarioDocente;
import br.edu.catolicato.sigeplane.domain.Disciplina;
import br.edu.catolicato.sigeplane.domain.Docente;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioDocenteService;
import br.edu.catolicato.sigeplane.servicos.gestaoDisciplina.DisciplinaService;
import br.edu.catolicato.sigeplane.servicos.gestaoUsuarios.DocenteService;
import com.csvreader.CsvReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 06 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class ImportCalendDocente implements Serializable {
    private static final long serialVersionUID = 1L;
    private Logger log;

    //----------- Serviços e Repositorio ----------------------
    @Inject
    private DocenteService docenteService;
    @Inject
    private DisciplinaService disciplinaService;
    @Inject
    private CalendarioDocenteService calendarioDocenteService;

    //--------------- Atributos -------------------------------
    @Inject
    private CalendarioDocente calendarioDocente;
    private List<CalendarioDocente> listaHorario;

    @PostConstruct
    public void init() {
        log = LogManager.getLogger(ImportCalendDocente.class.getName());
        listaHorario = new LinkedList<>();
    }

    public void setRepositorio(Repositorio repositorio) {
        this.docenteService.setRepositorio(repositorio);
        this.disciplinaService.setRepositorio(repositorio);
        this.calendarioDocenteService.setRepositorio(repositorio);
    }

    public List<CalendarioDocente> lerArquivo(UploadedFile file, CalendarioAcademico calendarioAcademico) {
        try {
            CsvReader csvReader = new CsvReader(file.getInputstream(), Charset.forName("UTF-8"));
            csvReader.setDelimiter(';');
            csvReader.readHeaders();
            Docente docente = null;
            listaHorario = new LinkedList<>();
            int linha = 2;
            while (csvReader.readRecord()) {
                String matriculaDocente = csvReader.get("matricula_docente");
                String codigoDisciplina = csvReader.get("codigo_disciplina");
                log.info("Lendo linha " + linha);
                if (!matriculaDocente.isEmpty() && !codigoDisciplina.isEmpty()) {
                    /*Verifica se o docente mudou*/
                    if (docente == null || !docente.getMatricula().equalsIgnoreCase(matriculaDocente)) {
                        listaHorario.add(calendarioDocente);
                        docente = docenteService.findDocenteByMatricula(matriculaDocente);
                        if (docente != null) {
                            CalendarioDocente horarioAux = calendarioDocenteService.findHrByDocenteCalendario(docente, calendarioAcademico);
                            if (horarioAux != null) {
//                                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Erro!", "Erro ao ler a linha " + linha + ", já existe horário para o docente!");
                                return null;
                            }
                            calendarioDocente = new CalendarioDocente(calendarioAcademico, docente);
                        } else {
//                            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Erro!", "Erro ao ler a linha " + linha + ", o docente informado não existe na base de dados!");
                            return null;
                        }
                    }

                    Disciplina disciplina = disciplinaService.findDisciplinaByCodigo(codigoDisciplina);
                    if (disciplina != null) {
                        calendarioDocente.addDisciplina(disciplina);
                    } else {
//                        FacesUtil.mostraMensagem(FacesMessage.SEVERITY_WARN, "Erro ao ler a linha " + linha + ", a disciplina informada não existe na base de dados!");
                        return null;
                    }

                } else {
//                    FacesUtil.mostraMensagem(FacesMessage.SEVERITY_WARN, "Erro ao ler a linha " + linha + ", verifique os dados informados!");
                }
                linha++;
            }

            if (!listaHorario.contains(calendarioDocente)) {
                listaHorario.add(calendarioDocente);
            }
            return listaHorario;
        } catch (IOException ex) {
//            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_FATAL, "Erro ao ler o arquivo! Verifique se o mesmo está de acordo com o layout estabelecido pelo sistema!");
            log.error("Erro ao importar registros do arquivo: " + file.getFileName());
            log.error(ex.getMessage());
            return null;
        }
    }// TODO -> método deve retornar int e mensagens devem ser de responsabilidade do controller

    public boolean salvarDados() {
        return calendarioDocenteService.salvarHorarios(listaHorario);
    }
}
