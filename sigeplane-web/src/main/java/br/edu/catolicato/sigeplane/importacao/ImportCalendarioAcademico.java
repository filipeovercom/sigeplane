package br.edu.catolicato.sigeplane.importacao;

import br.edu.catolicato.sigeplane.domain.AulaCalendario;
import br.edu.catolicato.sigeplane.domain.CalendarioAcademico;
import br.edu.catolicato.sigeplane.domain.StatusCalendario;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioAcademicoService;
import br.edu.catolicato.sigeplane.servicos.util.ServicosUteis;
import com.csvreader.CsvReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 06 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class ImportCalendarioAcademico implements Serializable {
    private static final long serialVersionUID = 1L;
    private Logger log;

    @Inject
    private CalendarioAcademico calendarioAcademico;
    @Inject
    private CalendarioAcademicoService calendarioAcademicoService;
    private Repositorio repositorio;

    @PostConstruct
    public void init() {
        log = LogManager.getLogger(ImportCalendarioAcademico.class.getName());
    }

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
        this.calendarioAcademicoService.setRepositorio(repositorio);
    }

    public List<AulaCalendario> lerArquivo(UploadedFile file) {
        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            CsvReader csvReader = new CsvReader(file.getInputstream(), Charset.forName("UTF-8"));
            csvReader.setDelimiter(';');
            csvReader.readHeaders();
            int linha = 2;
            while (csvReader.readRecord()) {
                String diaSemana = csvReader.get("dia_semana");
                String data = csvReader.get("data");
                String tipoAula = csvReader.get("tipo_aula");

                log.info("Lendo linha " + linha);
                if (!diaSemana.isEmpty() && !data.isEmpty() && !tipoAula.isEmpty()) {
                    try {
                        Date dataAux = (Date) formatter.parse(data);
                        calendarioAcademico.addAulaCalendario(new AulaCalendario(ServicosUteis.getDiaSemanaByString(diaSemana), dataAux,
                                ServicosUteis.getTipoAulaByString(tipoAula)));
                    } catch (ParseException e) {
//                        FacesUtil.mostraMensagem(FacesMessage.SEVERITY_WARN, "Erro ao ler a linha " + linha + ", verifique se a data segue o padrão dd/MM/aaaa!");
                        log.error("Erro ao importar registros do arquivo: " + file.getFileName());
                        log.error(e.getMessage());
                        return null;
                    }
                } else {
//                    FacesUtil.mostraMensagem(FacesMessage.SEVERITY_WARN, "Erro ao ler a linha " + linha + ", verifique os dados informados!");
                }
                linha++;
            }
            return calendarioAcademico.getAulasCalendario();
        } catch (IOException ex) {
//            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_FATAL, "Erro ao ler o arquivo! Verifique se o mesmo está de acordo com o layout estabelecido pelo sistema!");
            log.error("Erro ao importar registros do arquivo: " + file.getFileName());
            log.error(ex.getMessage());
            return null;
        }
    } // TODO -> método deve retornar int e mensagens devem ser de responsabilidade do controller

    public boolean salvarDados(String referencia) {
        calendarioAcademico.setReferencia(referencia);
        calendarioAcademico.setStatus(StatusCalendario.INICIADO);
        switch (calendarioAcademicoService.salvarCalendario(calendarioAcademico)) {
            case 3:
                return true;
            default:
                return false;
        }
    }
}
