package br.edu.catolicato.sigeplane.importacao;

import br.edu.catolicato.sigeplane.domain.Docente;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.LeituraCSV;
import br.edu.catolicato.sigeplane.servicos.gestaoUsuarios.DocenteService;
import com.csvreader.CsvReader;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 06 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class ImportTitulacaoProfessor implements LeituraCSV<Docente>, Serializable {
    private static final long serialVersionUID = 1L;
    private Logger log;
    private List<Docente> docentesEditar;

    @Inject
    private DocenteService docenteService;
    private Repositorio repositorio;

    @PostConstruct
    public void init() {
        log = LogManager.getLogger(ImportTitulacaoProfessor.class.getName());
    }

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
        this.docenteService.setRepositorio(repositorio);
    }

    @Override
    public List<Docente> lerArquivo(UploadedFile file) {
        try {
            docentesEditar = new LinkedList<>();

            CsvReader csvReader = new CsvReader(file.getInputstream(), Charset.defaultCharset());
            csvReader.setDelimiter(';');
            csvReader.readHeaders();
            int linha = 1;
            while (csvReader.readRecord()) {
                String matricula = csvReader.get("matricula");
                String bacharelado = csvReader.get("bacharelado");
                String licenciatura = csvReader.get("licenciatura");
                String tecnologo = csvReader.get("tecnologo");
                String especializacao = csvReader.get("especializacao");
                String mestrado = csvReader.get("mestrado");
                String doutorado = csvReader.get("doutorado");

                log.info("Lendo linha " + linha);
                if (!matricula.isEmpty()) {
                    Docente aux = docenteService.findDocenteByMatricula(matricula);
                    if (aux == null) {
//                        FacesUtil.mostraMensagem(FacesMessage.SEVERITY_WARN, "Erro ao ler a linha " + linha + ", o docente não existe no banco de dados!");
                    } else {
                        aux.setDoutorado(doutorado);
                        aux.setMestrado(mestrado);
                        aux.setEspecializacao(especializacao);
                        aux.setLicenciatura(licenciatura);
                        aux.setTecnologo(tecnologo);
                        aux.setBacharelado(bacharelado);
                        docentesEditar.add(aux);
                    }
                } else {
//                    FacesUtil.mostraMensagem(FacesMessage.SEVERITY_WARN, "Erro ao ler a linha " + linha + ", verifique os dados informados!");
                }
                linha++;
            }
            return docentesEditar;
        } catch (IOException ex) {
//            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_FATAL, "Erro ao ler o arquivo! Verifique se o mesmo está de acordo com o layout estabelecido pelo sistema!");
            log.error("Erro ao importar registros do arquivo: " + file.getFileName());
            log.error(ex.getMessage(), Level.ERROR);
            return null;
        }
    } // TODO -> método deve retornar int e mensagens devem ser de responsabilidade do controller

    @Override
    public boolean salvarDados() {
        if (docentesEditar != null && docentesEditar.size() > 0) {
            if (!docenteService.editarDocentes(docentesEditar)) {
                return false;
            }
        }
        return true;
    }
}
