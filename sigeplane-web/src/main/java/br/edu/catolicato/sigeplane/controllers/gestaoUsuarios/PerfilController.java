package br.edu.catolicato.sigeplane.controllers.gestaoUsuarios;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.CroppedImage;
import org.primefaces.model.UploadedFile;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Criado por Filipe D. Abreu em 16 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Named
@ViewScoped
public class PerfilController implements Serializable {
    private static final long serialVersionUID = 1L;

    private CroppedImage imagemCortada;
    private UploadedFile imagem;

    public void fileUploadListener(FileUploadEvent event) {
        imagem = event.getFile();
        RequestContext.getCurrentInstance().update("cropImagem");
    }

    public CroppedImage getImagemCortada() {
        return imagemCortada;
    }

    public void setImagemCortada(CroppedImage imagemCortada) {
        this.imagemCortada = imagemCortada;
    }

    public UploadedFile getImagem() {
        return imagem;
    }

    public void setImagem(UploadedFile imagem) {
        this.imagem = imagem;
    }
}
