package br.edu.catolicato.sigeplane.seguranca;

import br.edu.catolicato.sigeplane.domain.Perfil;
import br.edu.catolicato.sigeplane.domain.Usuario;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.envioEmail.EnvioEmailService;
import br.edu.catolicato.sigeplane.servicos.seguranca.SegurancaService;
import br.edu.catolicato.sigeplane.util.CriptografiaSHA;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.Serializable;

/**
 * Criado por Filipe D. Abreu em 31 de Agosto de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class ControleDeSeguranca implements Serializable {
    public static final String USUARIO_LOGADO = "usuario_logado";
    private static final long serialVersionUID = 1L;
    @EJB
    private Repositorio repositorio;
    @Inject
    private SegurancaService segurancaService;

    @PostConstruct
    public void init(){
        this.segurancaService.setRepositorio(repositorio);
    }

    public void colocaUsuarioNaSessao(String matricula) {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Usuario user = segurancaService.findUsuarioByMatricula(matricula);
        session.setAttribute(USUARIO_LOGADO, user);
    }

    public boolean isPrimeiroAcesso() throws IOException {
        if (!getUsuarioLogado().getPerfil().equals(Perfil.DISCENTE)
                && CriptografiaSHA.hash256(getUsuarioLogado().getMatricula()).equalsIgnoreCase(getUsuarioLogado().getSenha())) {
            return true;
        }
        return false;
    }

    public Usuario getUsuarioLogado() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        return (Usuario) session.getAttribute(USUARIO_LOGADO);
    }

    public boolean redefineSenhaUser(Usuario user) {
        if (repositorio.editar(user)) {
            return true;
        }
        return false;
    }

    public boolean enviarSenhaUserPorEmail(String email) {
        Usuario u = segurancaService.findUsuarioByEmail(email);
        boolean ret = false;
        if (u != null) {
            // TODO -> Redefinir a senha do usuário para a matrícula
            ret = EnvioEmailService.enviarEmailSimples(
                    "Sua senha do sistema SIGEPLANE",
                    email, "<h4>Olá, esta é sua nova senha no sistema <strong>SIGEPLANE</strong></h4>"
                            + "<br/>"
                            + "<h2>" + u.getMatricula() + "</h2>"
            );
        }
        return ret;
    }
}
