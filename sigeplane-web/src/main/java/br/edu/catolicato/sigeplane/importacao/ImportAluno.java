package br.edu.catolicato.sigeplane.importacao;

import br.edu.catolicato.sigeplane.domain.Discente;
import br.edu.catolicato.sigeplane.domain.Perfil;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.LeituraCSV;
import br.edu.catolicato.sigeplane.servicos.gestaoUsuarios.DiscenteService;
import br.edu.catolicato.sigeplane.util.CriptografiaSHA;
import com.csvreader.CsvReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 06 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class ImportAluno implements LeituraCSV<Discente>, Serializable {
    private static final long serialVersionUID = 1L;
    private Logger log;
    private List<Discente> discentesSalvar;
    private List<Discente> discentesEditar;

    @Inject
    private DiscenteService discenteService;
    private Repositorio repositorio;

    @PostConstruct
    public void init() {
        log = LogManager.getLogger(ImportAluno.class.getName());
    }

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    @Override
    public List<Discente> lerArquivo(UploadedFile file) {
        try {
            discentesSalvar = new LinkedList<>();
            discentesEditar = new LinkedList<>();

            CsvReader csvReader = new CsvReader(file.getInputstream(), Charset.forName("UTF-8"));
            csvReader.setDelimiter(';');
            csvReader.readHeaders();
            int linha = 1;
            while (csvReader.readRecord()) {
                String matricula = csvReader.get("matricula");
                String email = csvReader.get("email");
                String nome = csvReader.get("nome");

                log.info("Lendo linha " + linha);
                if (!matricula.isEmpty() && !email.isEmpty() && !nome.isEmpty()) {
                    Discente aux = discenteService.findDiscenteByMatricula(matricula);
                    if (aux != null) {
                        aux.setNome(nome);
                        aux.setMatricula(matricula);
                        aux.setSenha(CriptografiaSHA.hash256(matricula));
                        aux.setEmail(email);
                        discentesEditar.add(aux);
                    } else {
                        discentesSalvar.add(
                                new Discente(
                                        nome, matricula,
                                        CriptografiaSHA.hash256(matricula),
                                        matricula, email, Perfil.DISCENTE, false
                                )
                        );
                    }
                } else {
//                    FacesUtil.mostraMensagem(FacesMessage.SEVERITY_WARN, "Erro ao ler a linha " + linha + ", verifique os dados informados!");
                }
                linha++;
            }
            List<Discente> retorno = new LinkedList<>();
            retorno.addAll(discentesEditar);
            retorno.addAll(discentesSalvar);
            return retorno;
        } catch (IOException ex) {
//            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_FATAL, "Erro ao ler o arquivo! Verifique se o mesmo está de acordo com o layout estabelecido pelo sistema!");
            log.error("Erro ao importar registros do arquivo: " + file.getFileName());
            log.error(ex.getMessage());
            return null;
        }
    } // TODO -> método deve retornar int e mensagens devem ser de responsabilidade do controller

    @Override
    public boolean salvarDados() {
        boolean ret = true;
        if (discentesSalvar != null && discentesSalvar.size() > 0) {
            if (!discenteService.salvarDiscentes(discentesSalvar)) {
                ret = false;
            }
        }

        if (ret) {
            if (discentesEditar != null && discentesEditar.size() > 0) {
                if (!discenteService.editarDiscentes(discentesEditar)) {
                    ret = false;
                }
            }
        }

        return ret;
    }
}
