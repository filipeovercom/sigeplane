package br.edu.catolicato.sigeplane.controllers.gestaoUsuarios;

import br.edu.catolicato.sigeplane.domain.Discente;
import br.edu.catolicato.sigeplane.importacao.ImportAluno;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 07 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Named
@ViewScoped
public class CargaDiscenteController implements Serializable {
    private static final long serialVersionUID = 1L;

    @EJB
    private Repositorio repositorio;
    @Inject
    private ImportAluno importAluno;
    private List<Discente> discentes;
    private UploadedFile uploadedFile;

    @PostConstruct
    public void init() {
        this.importAluno.setRepositorio(repositorio);
        discentes = new LinkedList<>();
    }

    public List<Discente> getDiscentes() {
        return discentes;
    }

    public void setDiscentes(List<Discente> discentes) {
        this.discentes = discentes;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public void fileUploadListener(FileUploadEvent event) {
        uploadedFile = event.getFile();
        lerArquivo();
        RequestContext.getCurrentInstance().update("tabelaDiscentes");
    }

    public void lerArquivo() {
        if (uploadedFile != null) {
            discentes = importAluno.lerArquivo(this.uploadedFile);
            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Foram lidos " + discentes.size() + " registro(s)");
        }
    }

    public void salvarDados() {
        if (discentes.size() > 0) {
            boolean ret = importAluno.salvarDados();
            if (ret) {
                discentes = new LinkedList<>();
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Registros salvos com sucesso!");
            }
        }
    }

}
