package br.edu.catolicato.sigeplane.controllers.gestaoDisciplinas;

import br.edu.catolicato.sigeplane.domain.Disciplina;
import br.edu.catolicato.sigeplane.importacao.ImportDisciplina;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 07 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Named
@ViewScoped
public class CargaDisciplinaController implements Serializable {
    private static final long serialVersionUID = 1L;

    @EJB
    private Repositorio repositorio;

    @Inject
    private ImportDisciplina importDisciplina;
    private List<Disciplina> disciplinas;
    private UploadedFile uploadedFile;

    @PostConstruct
    public void init() {
        this.importDisciplina.setRepositorio(repositorio);
        disciplinas = new LinkedList<>();
    }

    public List<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public void fileUploadListener(FileUploadEvent event) {
        uploadedFile = event.getFile();
        lerArquivo();
        RequestContext.getCurrentInstance().update("tabelaDisciplinas");
    }

    public void lerArquivo() {
        if (uploadedFile != null) {
            disciplinas = importDisciplina.lerArquivo(this.uploadedFile);
            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Foram lidos " + disciplinas.size() + " registro(s)");
        }
    }

    public void salvarDados() {
        if (disciplinas.size() > 0) {
            boolean ret = importDisciplina.salvarDados();
            if (ret) {
                disciplinas = new LinkedList<>();
                FacesUtil.mostraMensagem(FacesMessage.SEVERITY_INFO, "Registros salvos com sucesso!");
            }
        }
    }

}
