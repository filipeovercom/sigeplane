package br.edu.catolicato.sigeplane.controllers.calendarioacademico;

import br.edu.catolicato.sigeplane.domain.CalendarioAcademico;
import br.edu.catolicato.sigeplane.domain.CalendarioDocente;
import br.edu.catolicato.sigeplane.domain.Disciplina;
import br.edu.catolicato.sigeplane.domain.Docente;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioAcademicoService;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioDocenteService;
import br.edu.catolicato.sigeplane.servicos.gestaoDisciplina.DisciplinaService;
import br.edu.catolicato.sigeplane.servicos.gestaoUsuarios.DocenteService;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DualListModel;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 24 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Named
@ViewScoped
public class AlteracoesCalendDocenteController implements Serializable {
    private static final long serialVersionUID = 1L;

    //-------------- Serviços e Repositorio ----------------------
    @EJB
    private Repositorio repositorio;
    @Inject
    private CalendarioDocenteService calendarioDocenteService;
    @Inject
    private DocenteService docenteService;
    @Inject
    private DisciplinaService disciplinaService;
    @Inject
    private CalendarioAcademicoService calendService;

    //--------------- Atributos ------------------------------------
    @Inject
    private CalendarioDocente novoHorario;
    private CalendarioDocente horarioSelecionado;
    private List<CalendarioDocente> listaHorarios;
    private List<CalendarioDocente> listaHorariosFiltrados;
    private List<CalendarioAcademico> listaCalendarios;
    private List<Disciplina> disciplinasDisponiveis;
    private List<Docente> professores;
    private DualListModel<Disciplina> disciplinaDualListModel;

    @PostConstruct
    public void init() {
        this.calendarioDocenteService.setRepositorio(repositorio);
        this.disciplinaService.setRepositorio(repositorio);
        this.calendService.setRepositorio(repositorio);
        this.docenteService.setRepositorio(repositorio);
        listaHorarios = calendarioDocenteService.buscaTodos();
    }


    public void salvarHorario() {
        if (calendarioDocenteService.findHrByDocenteCalendario(novoHorario.getDocente(), novoHorario.getCalendarioAcademico()) == null) {
            novoHorario.getDisciplinas().addAll(disciplinaDualListModel.getTarget());
            novoHorario.getDocente().getHorarios().add(horarioSelecionado);
            for (Disciplina d : novoHorario.getDisciplinas()) {
                d.getHorariosDocentes().add(novoHorario);
            }
            novoHorario.getCalendarioAcademico().getDocentes().add(novoHorario);
            if (calendarioDocenteService.salvarHorario(novoHorario)) {
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Sucesso!", "Registro salvo com sucesso!");
                novoHorario = new CalendarioDocente();
                listaHorarios = null;
                disciplinaDualListModel = null;
                RequestContext.getCurrentInstance().update("tabelaHorarios");
                RequestContext.getCurrentInstance().update("conteudoFormAdd");
                RequestContext.getCurrentInstance().execute("fechaFormAdd();");
            } else {
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_ERROR, "Erro!", "Erro ao salvar Registro! Verifique os dados e Tente novamente!");
            }
        } else {
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Oops!", "O professor informado já tem horário para o semestre informado!");
        }
    }

    public void editarHorario() {
        this.horarioSelecionado.getDisciplinas().clear();
        this.horarioSelecionado.getDisciplinas().addAll(disciplinaDualListModel.getTarget());
        if (calendarioDocenteService.editarHorario(this.horarioSelecionado)) {
            horarioSelecionado = null;
            listaHorarios = null;
            FacesUtil.mostraMensagem("msgGlobal", FacesMessage.SEVERITY_INFO, "Sucesso!", "Registro editado com sucesso!");
            RequestContext context = RequestContext.getCurrentInstance();
            context.update("tabelaHorarios");
            context.update("panelMenu");
            context.execute("ocultaFormEditar();");
        } else {
            FacesUtil.mostraMensagem("formEditar", FacesMessage.SEVERITY_INFO, "Erro!", "Erro ao editar Registro! Atualize a página e Tente novamente!");
        }
    }

    public void excluirHorario() {
        if (calendarioDocenteService.excluirHorario(this.horarioSelecionado)) {
            FacesUtil.mostraMensagem("msgGlobal", FacesMessage.SEVERITY_INFO, "Sucesso!", "Registro excluido com sucesso!");
            horarioSelecionado = null;
            listaHorarios = null;
            RequestContext.getCurrentInstance().update("tabelaHorarios");
        } else {
            FacesUtil.mostraMensagem("msgGlobal", FacesMessage.SEVERITY_INFO, "Erro!", "Erro ao editar Registro! Atualize a página e Tente novamente!");
        }
    }

    public void inicializaPicklistDisciplinas() {
        if (novoHorario.getCalendarioAcademico() != null) {
            disciplinasDisponiveis = calendarioDocenteService.findDisciplinasDisponiveis(novoHorario.getCalendarioAcademico());
        }
        this.disciplinaDualListModel = new DualListModel<>(disciplinasDisponiveis, new ArrayList<Disciplina>());
    }

    public List<Docente> completeProfessor(String query) {
        if (professores == null) {
            professores = docenteService.buscaTodos();
        }
        List<Docente> profFiltrados = new LinkedList<>();
        for (int i = 0; i < professores.size(); i++) {
            Docente professor = professores.get(i);
            if (professor.getNome().toLowerCase().startsWith(query.toLowerCase())
                    || professor.getMatricula().toLowerCase().startsWith(query.toLowerCase())) {
                profFiltrados.add(professor);
            }
        }
        return profFiltrados;
    }

    public void inicializaFormEditar() {
        disciplinasDisponiveis = calendarioDocenteService.findDisciplinasDisponiveis(horarioSelecionado.getCalendarioAcademico());
        this.disciplinaDualListModel = new DualListModel<>(disciplinasDisponiveis, horarioSelecionado.getDisciplinas());
    }

    public void limpaFormEditar() {
        this.horarioSelecionado = null;
    }

    public CalendarioDocente getNovoHorario() {
        return novoHorario;
    }

    public void setNovoHorario(CalendarioDocente novoHorario) {
        this.novoHorario = novoHorario;
    }

    public CalendarioDocente getHorarioSelecionado() {
        return horarioSelecionado;
    }

    public void setHorarioSelecionado(CalendarioDocente horarioSelecionado) {
        this.horarioSelecionado = horarioSelecionado;
    }

    public List<CalendarioDocente> getListaHorarios() {
        if (listaHorarios == null) {
            listaHorarios = calendarioDocenteService.buscaTodos();
        }
        return listaHorarios;
    }

    public void setListaHorarios(List<CalendarioDocente> listaHorarios) {
        this.listaHorarios = listaHorarios;
    }

    public List<CalendarioDocente> getListaHorariosFiltrados() {
        return listaHorariosFiltrados;
    }

    public void setListaHorariosFiltrados(List<CalendarioDocente> listaHorariosFiltrados) {
        this.listaHorariosFiltrados = listaHorariosFiltrados;
    }

    public List<CalendarioAcademico> getListaCalendarios() {
        if (listaCalendarios == null) {
            listaCalendarios = calendService.buscaTodos();
        }
        return listaCalendarios;
    }

    public void setListaCalendarios(List<CalendarioAcademico> listaCalendarios) {
        this.listaCalendarios = listaCalendarios;
    }

    public DualListModel<Disciplina> getDisciplinaDualListModel() {
        if (disciplinaDualListModel == null) {
            disciplinaDualListModel = new DualListModel<>(new ArrayList(), new ArrayList());
        }
        return disciplinaDualListModel;
    }

    public void setDisciplinaDualListModel(DualListModel<Disciplina> disciplinaDualListModel) {
        this.disciplinaDualListModel = disciplinaDualListModel;
    }

    public List<Disciplina> getDisciplinasDisponiveis() {
        return disciplinasDisponiveis;
    }

    public void setDisciplinasDisponiveis(List<Disciplina> disciplinasDisponiveis) {
        this.disciplinasDisponiveis = disciplinasDisponiveis;
    }
}
