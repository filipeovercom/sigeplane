package br.edu.catolicato.sigeplane.importacao;

import br.edu.catolicato.sigeplane.domain.Curso;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.LeituraCSV;
import br.edu.catolicato.sigeplane.servicos.gestaoCurso.CursoService;
import com.csvreader.CsvReader;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 06 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class ImportCurso implements LeituraCSV<Curso>, Serializable {
    private static final long serialVersionUID = 1L;

    @Inject
    private CursoService cursoService;
    private Logger log;
    private List<Curso> cursosSalvar;
    private List<Curso> cursosEditar;
    private Repositorio repositorio;

    @PostConstruct
    public void init() {
        log = LogManager.getLogger(ImportCurso.class.getName());
    }

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
        this.cursoService.setRepositorio(repositorio);
    }

    @Override
    public List<Curso> lerArquivo(UploadedFile file) {
        try {
            cursosSalvar = new LinkedList<>();
            cursosEditar = new LinkedList<>();

            CsvReader csvReader = new CsvReader(file.getInputstream(), Charset.defaultCharset());
            csvReader.setDelimiter(';');
            csvReader.readHeaders();
            int linha = 1;
            while (csvReader.readRecord()) {
                String codigo = csvReader.get("codigo");
                String nome = csvReader.get("nome");
                String descricao = csvReader.get("descricao");
                log.info("Lendo linha " + linha);
                if (!nome.isEmpty()) {
                    Curso aux = null;
                    if (!codigo.isEmpty()) {
                        aux = cursoService.findCursoByCodigo(codigo);
                    } else {
                        aux = cursoService.findCursoByNome(nome);
                    }
                    if (aux != null) {
                        aux.setCodigo(codigo);
                        aux.setNome(nome);
                        aux.setDescricao(descricao);
                        cursosEditar.add(aux);
                    } else {
                        cursosSalvar.add(
                                new Curso(
                                        nome, codigo, descricao
                                )
                        );
                    }
                } else {
//                    FacesUtil.mostraMensagem(FacesMessage.SEVERITY_WARN, "Erro ao ler a linha " + linha + ", verifique os dados informados!");
                }
                linha++;
            }
            List<Curso> retorno = new LinkedList<>();
            retorno.addAll(cursosEditar);
            retorno.addAll(cursosSalvar);
            return retorno;
        } catch (IOException ex) {
//            FacesUtil.mostraMensagem(FacesMessage.SEVERITY_FATAL, "Erro ao ler o arquivo! Verifique se o mesmo está de acordo com o layout estabelecido pelo sistema!");
            log.error("Erro ao importar registros do arquivo: " + file.getFileName());
            log.error(ex.getMessage(), Level.ERROR);
            return null;
        }
    } // TODO -> método deve retornar int e mensagens devem ser de responsabilidade do controller

    @Override
    public boolean salvarDados() {
        boolean ret = true;
        if (cursosSalvar != null && cursosSalvar.size() > 0) {
            if (!cursoService.salvarCursos(cursosSalvar)) {
                ret = false;
            }
        }
        if (ret) {
            if (cursosEditar != null && cursosEditar.size() > 0) {
                if (!cursoService.editarCursos(cursosEditar)) {
                    ret = false;
                }
            }
        }
        return ret;
    }
}
