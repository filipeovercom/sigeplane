package br.edu.catolicato.sigeplane.controllers.planoEnsino;

import br.edu.catolicato.sigeplane.domain.*;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.seguranca.ControleDeSeguranca;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioAcademicoService;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioDocenteService;
import br.edu.catolicato.sigeplane.servicos.gestaoDisciplina.DisciplinaService;
import br.edu.catolicato.sigeplane.servicos.gestaoPlanoEnsino.PlanoEnsinoService;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.LinkedList;

/**
 * Criado por Filipe D. Abreu em 07 de Setembro de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Named
@ViewScoped
public class AtualizacaoPlanoEnsinoController implements Serializable {
    private static final long serialVersionUID = 1L;

    /*****************
     * SERVIÇOS
     *****************/
    @EJB
    private Repositorio repositorio;
    @Inject
    private PlanoEnsinoService planoEnsinoService;
    @Inject
    private DisciplinaService disciplinaService;
    @Inject
    private CalendarioDocenteService calendarioDocenteService;
    @Inject
    private CalendarioAcademicoService calendarioAcademicoService;
    @Inject
    private ControleDeSeguranca controleDeSeguranca;
    /**************************************************/
    /****************
     * ATRIBUTOS
     ****************/
    private String idDisciplinaURL;
    private String idCalendAcadURL;
    private PlanoEnsino vrPlanoEnsino;
    private Disciplina vrDisciplina;
    private CalendarioAcademico vrCalendarioAcademico;
    private Docente vrProfessor;
    private boolean edicao;

    @PostConstruct
    public void init() {
        this.planoEnsinoService.setRepositorio(repositorio);
        this.disciplinaService.setRepositorio(repositorio);
        this.calendarioDocenteService.setRepositorio(repositorio);
        this.calendarioAcademicoService.setRepositorio(repositorio);
    }

    /**************************************************/
    public void inicializaEdicao() {
        boolean redireciona = false;
        if (vrPlanoEnsino == null) {
            try {
                vrDisciplina = disciplinaService.buscaPorId(Integer.valueOf(idDisciplinaURL));
                vrCalendarioAcademico = calendarioAcademicoService.buscaPorId(Integer.valueOf(idCalendAcadURL));
                if (vrDisciplina == null || vrCalendarioAcademico == null) {
                    FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Oops!", "Disciplina ou Calendário informados na URL não existem na base de dados!");
                    redireciona = true;
                } else {
                    vrPlanoEnsino = planoEnsinoService.planoEnsinoByDisciplinaAndCalend(vrDisciplina, vrCalendarioAcademico);
                    vrProfessor = (Docente) controleDeSeguranca.getUsuarioLogado();
                    if (vrPlanoEnsino == null) {
                        edicao = false;
                        vrPlanoEnsino = new PlanoEnsino();
                        // Verifica se a disciplina já tem plano de ensino cadastrado
                        if (vrDisciplina.getPlanosEnsino().size() > 0) {
                            // Se tiver pega a última Atualização
                            PlanoEnsino p = vrDisciplina.getPlanosEnsino().get(vrDisciplina.getPlanosEnsino().size() - 1);
                            vrPlanoEnsino.setBibliografiaBasica(p.getBibliografiaBasica());
                            vrPlanoEnsino.setBibliografiaComplementar(p.getBibliografiaComplementar());
                            for (CompetenciaPlano cp : p.getCompetencias()) {
                                vrPlanoEnsino.getCompetencias().add(new CompetenciaPlano(cp.getCompetencia(), vrPlanoEnsino));
                            }
                            vrPlanoEnsino.setGuardaNumExibicoes(p.isGuardaNumExibicoes());
                            vrPlanoEnsino.setStatus(StatusPlanoEnsino.ABERTO);
                        } else {
                            vrPlanoEnsino.setDisciplina(vrDisciplina);
                            vrPlanoEnsino.setStatus(StatusPlanoEnsino.ABERTO);
                            vrPlanoEnsino.setGuardaNumExibicoes(false);
                            vrPlanoEnsino.setBibliografiaBasica(new LinkedList<Livro>());
                        }
                        vrDisciplina.addPlanoEnsino(vrPlanoEnsino);
                        vrCalendarioAcademico.addPlanoEnsino(vrPlanoEnsino);
                        vrProfessor.addPlanoEnsino(vrPlanoEnsino);
                    }
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        if (redireciona) {
            RequestContext.getCurrentInstance().execute("location.href='http://localhost:8080/sigeplane/inicio';");
        }
    }

    public String getIdDisciplinaURL() {
        return idDisciplinaURL;
    }

    public void setIdDisciplinaURL(String idDisciplinaURL) {
        this.idDisciplinaURL = idDisciplinaURL;
    }

    public String getIdCalendAcadURL() {
        return idCalendAcadURL;
    }

    public void setIdCalendAcadURL(String idCalendAcadURL) {
        this.idCalendAcadURL = idCalendAcadURL;
    }

    public PlanoEnsino getVrPlanoEnsino() {
        return vrPlanoEnsino;
    }

    public void setVrPlanoEnsino(PlanoEnsino vrPlanoEnsino) {
        this.vrPlanoEnsino = vrPlanoEnsino;
    }

    public Disciplina getVrDisciplina() {
        return vrDisciplina;
    }

    public void setVrDisciplina(Disciplina vrDisciplina) {
        this.vrDisciplina = vrDisciplina;
    }

    public CalendarioAcademico getVrCalendarioAcademico() {
        return vrCalendarioAcademico;
    }

    public void setVrCalendarioAcademico(CalendarioAcademico vrCalendarioAcademico) {
        this.vrCalendarioAcademico = vrCalendarioAcademico;
    }

    public Docente getVrProfessor() {
        return vrProfessor;
    }

    public void setVrProfessor(Docente vrProfessor) {
        this.vrProfessor = vrProfessor;
    }

    public boolean isEdicao() {
        return edicao;
    }

    public void setEdicao(boolean edicao) {
        this.edicao = edicao;
    }
}
