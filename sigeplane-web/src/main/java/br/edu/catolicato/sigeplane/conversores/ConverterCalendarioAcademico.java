package br.edu.catolicato.sigeplane.conversores;

import br.edu.catolicato.sigeplane.domain.CalendarioAcademico;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Criado por Filipe D. Abreu em 17 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@FacesConverter(value = "converterCalendarioAcad")
public class ConverterCalendarioAcademico implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null && value.trim().length() > 0 && !value.equalsIgnoreCase("Selecione um calendario...")) {
            try {
                return component.getAttributes().get(value);
            } catch (NumberFormatException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != "" && value != null) {
            CalendarioAcademico aux = (CalendarioAcademico) value;
            String id = String.valueOf(aux.getId());
            if (aux.getId() != null) {
                component.getAttributes().put(id, aux);
            }
            return id;
        } else {
            return null;
        }
    }
}
