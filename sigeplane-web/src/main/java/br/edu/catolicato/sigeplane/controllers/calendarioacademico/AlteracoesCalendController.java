package br.edu.catolicato.sigeplane.controllers.calendarioacademico;

import br.edu.catolicato.sigeplane.domain.*;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioAcademicoService;
import br.edu.catolicato.sigeplane.servicos.util.ServicosUteis;
import br.edu.catolicato.sigeplane.util.FacesUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 01 de Agosto de 2015. SIGEPLANE - Sistema de
 * Gestão de Plano de Ensino Faculdade Católica do Tocantins - Sistemas de
 * Informação
 */
@Named
@ViewScoped
public class AlteracoesCalendController implements Serializable {

    private static final long serialVersionUID = 1L;

    //-------------------- Serviços e Repositorio -------------------------------
    @EJB
    private Repositorio repositorio;
    @Inject
    private CalendarioAcademicoService service;
    //---------------------------------------------------------------------------
    //--------------------- Atributos -------------------------------------------
    @Inject
    private AulaCalendario aulaAuxiliar;
    private CalendarioAcademico calendAcadSelecionado;
    private List<CalendarioAcademico> lista;
    private List<CalendarioAcademico> listaFiltrada;
    private TipoAula[] tiposAula;
    private DiaSemana[] diasSemana;
    private StatusCalendario[] listStatusCalend;
    private Integer anoSemestre;
    private Integer periodoSemestre;
    private Logger log;
    private StatusCalendario vrNewStatus;
    //---------------------------------------------------------------------------

    @PostConstruct
    public void init() {
        log = LogManager.getLogger(AlteracoesCalendController.class);
        this.service.setRepositorio(repositorio);
    }

    public void editarCalendario() {
        if (service.editarCalendario(calendAcadSelecionado)) {
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Sucesso!", "Calendário " + calendAcadSelecionado.getReferencia() + " editado com sucesso!");
            calendAcadSelecionado = null;
            lista = null;
            anoSemestre = 0;
            periodoSemestre = 0;
            RequestContext context = RequestContext.getCurrentInstance();
            context.update("tabelaCalendarios");
            context.update("div_menus");
            context.execute("ocultaFormEditar()");
        }
    }

    public void excluirCalendario() {
        if (service.excluirCalendario(calendAcadSelecionado)) {
            calendAcadSelecionado = null;
            lista = null;
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Sucesso!", "Registro excluído com sucesso!");
            RequestContext.getCurrentInstance().update("tabelaCalendarios");
            RequestContext.getCurrentInstance().update("div_menus");
        } else {
            FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Erro!", "Erro ao excluir o registro! Tente novamente!");
        }
    }

    public void mudarStatusCalendario() {
        int result = service.mudarStatusCalendario(calendAcadSelecionado, vrNewStatus);
        switch (result) {
            case -4:
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_ERROR, "Erro!", "Erro ao reabrir planos de ensino do calendário. Atualize a página e tente novamente!");
                break;
            case -3:
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_ERROR, "Erro!", "Erro ao realizar a operação. Atualize a página e tente novamente!");
                break;
            case -2:
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_ERROR, "Erro!", "Erro ao fechar calendário. Atualize a página e tente novamente!");
                break;
            case -1:
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Oops!", "Já existe um calendário com status Em Andamento!");
                break;
            case 1:
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Sucesso!", "Status do calendário "+calendAcadSelecionado.getReferencia()+" atualizado com sucesso!");
                calendAcadSelecionado = null;
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("PF('dialogAtivarCalend').hide();");
                context.update("tabelaCalendarios");
                context.update("div_menus");
                context.update("statusCalend");
                break;
        }
    }

    public void limparFormAdicionar() {
    }

    public void limpaFormAtivarCalend() {
        calendAcadSelecionado = null;
    }

    public void limparFormEditar() {
        this.calendAcadSelecionado = null;
    }

    public void adicionaAulaCalendario() {
        if (calendAcadSelecionado != null) {
            if (!calendAcadSelecionado.getAulasCalendario().contains(aulaAuxiliar)
                    && aulaAuxiliar.getDataAula() != null) {
                DiaSemana dia = ServicosUteis.getDiaSemanaByDate(aulaAuxiliar.getDataAula());
                if (!dia.equals(aulaAuxiliar.getDiaSemana()) && aulaAuxiliar.getTipoAula().equals(TipoAula.AULA_NORMAL)) {
                    FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Oops!", "Quando o <strong>tipo de aula</strong> é normal a <strong>data da aula</strong> deve ser igual ao <strong>Dia da Semana!</strong>");
                } else {
                    calendAcadSelecionado.getAulasCalendario().add(aulaAuxiliar);
                    FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_INFO, "Sucesso!", "Aula adicionada com sucesso!");
                }
            } else if (calendAcadSelecionado.getAulasCalendario().contains(aulaAuxiliar)) {
                FacesUtil.mostraMensagem(null, FacesMessage.SEVERITY_WARN, "Oops!", "Aula já existe no calendário!");
            }
        }
    }

    public void removeAulaCalendario(AulaCalendario pAulaCalendario) {
        if (calendAcadSelecionado != null) {
            calendAcadSelecionado.getAulasCalendario().remove(pAulaCalendario);
        }
    }

    public CalendarioAcademico getCalendAcadSelecionado() {
        return calendAcadSelecionado;
    }

    public void setCalendAcadSelecionado(CalendarioAcademico calendAcadSelecionado) {
        this.calendAcadSelecionado = calendAcadSelecionado;
    }

    public List<CalendarioAcademico> getLista() {
        if (lista == null) {
            lista = service.buscaTodos();
        }
        return lista;
    }

    public void setLista(List<CalendarioAcademico> lista) {
        this.lista = lista;
    }

    public List<CalendarioAcademico> getListaFiltrada() {
        return listaFiltrada;
    }

    public void setListaFiltrada(List<CalendarioAcademico> listaFiltrada) {
        this.listaFiltrada = listaFiltrada;
    }

    public AulaCalendario getAulaAuxiliar() {
        return aulaAuxiliar;
    }

    public void setAulaAuxiliar(AulaCalendario aulaAuxiliar) {
        this.aulaAuxiliar = aulaAuxiliar;
    }

    public Integer getAnoSemestre() {
        return anoSemestre;
    }

    public void setAnoSemestre(Integer anoSemestre) {
        this.anoSemestre = anoSemestre;
    }

    public Integer getPeriodoSemestre() {
        return periodoSemestre;
    }

    public void setPeriodoSemestre(Integer periodoSemestre) {
        this.periodoSemestre = periodoSemestre;
    }

    public CalendarioAcademicoService getService() {
        return service;
    }

    public void setService(CalendarioAcademicoService service) {
        this.service = service;
    }

    public TipoAula[] getTiposAula() {
        if (tiposAula == null) {
            tiposAula = TipoAula.values();
        }
        return tiposAula;
    }

    public void setTiposAula(TipoAula[] tiposAula) {
        this.tiposAula = tiposAula;
    }

    public DiaSemana[] getDiasSemana() {
        if (diasSemana == null) {
            diasSemana = DiaSemana.values();
        }
        return diasSemana;
    }

    public void setDiasSemana(DiaSemana[] diasSemana) {
        this.diasSemana = diasSemana;
    }


    public StatusCalendario[] getListStatusCalend() {
        if (listStatusCalend == null) {
            StatusCalendario[] aux = StatusCalendario.values();
            listStatusCalend = new StatusCalendario[aux.length - 1];
            int pos = 0;
            for (int i = 0; i < aux.length; i++) {
                if (!aux[i].equals(calendAcadSelecionado.getStatus())) {
                    listStatusCalend[pos] = aux[i];
                    pos++;
                }
            }
        }
        return listStatusCalend;
    }

    public void setListStatusCalend(StatusCalendario[] listStatusCalend) {
        this.listStatusCalend = listStatusCalend;
    }

    public StatusCalendario getVrNewStatus() {
        return vrNewStatus;
    }

    public void setVrNewStatus(StatusCalendario vrNewStatus) {
        this.vrNewStatus = vrNewStatus;
    }
}
