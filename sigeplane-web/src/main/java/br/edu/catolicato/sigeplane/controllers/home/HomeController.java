package br.edu.catolicato.sigeplane.controllers.home;

import br.edu.catolicato.sigeplane.domain.*;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.seguranca.ControleDeSeguranca;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioAcademicoService;
import br.edu.catolicato.sigeplane.servicos.gestaoPlanoEnsino.PlanoEnsinoService;
import br.edu.catolicato.sigeplane.servicos.gestaoUsuarios.DocenteService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Criado por Filipe D. Abreu em 27 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */

@Named
@ViewScoped
public class HomeController implements Serializable {
    private static final long serialVersionUID = 1L;

    @EJB
    private Repositorio repositorio;
    @Inject
    private DocenteService docenteService;
    @Inject
    private ControleDeSeguranca seguranca;
    @Inject
    private CalendarioAcademicoService calendService;
    @Inject
    private PlanoEnsinoService planoEnsinoService;
    private Docente docenteLogado;

    @PostConstruct
    public void init() {
        this.docenteService.setRepositorio(repositorio);
        this.calendService.setRepositorio(repositorio);
        this.planoEnsinoService.setRepositorio(repositorio);
    }

    public Docente getDocenteLogado() {
        if (docenteLogado == null) {
            docenteLogado = (Docente) seguranca.getUsuarioLogado();
        }
        return docenteLogado;
    }

    public void setDocenteLogado(Docente docenteLogado) {
        this.docenteLogado = docenteLogado;
    }

    public String statusPlanoEnsino(Disciplina disciplina, CalendarioAcademico calendarioAcademico) {
        StatusPlanoEnsino s = planoEnsinoService.statusDoPlanoByDisciplinaAndCalend(disciplina, calendarioAcademico);
        if (s != null) {
            return s.getDescricao();
        } else {
            return "Aguardando Atualização!";
        }
    }

    public boolean isEditaPlanoEnsino(Disciplina disciplina, CalendarioAcademico calendarioAcademico) {
        PlanoEnsino p = planoEnsinoService.planoEnsinoByDisciplinaAndCalend(disciplina, calendarioAcademico);
        if (p == null) {
            return true;
        } else if (p.getStatus().equals(StatusPlanoEnsino.AGUARDANDO_APROVACAO) || p.getStatus().equals(StatusPlanoEnsino.FECHADO)) {
            return false;
        }
        return true;
    }

}
