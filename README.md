# README #

# Sigeplane - Sistema de Gestão de Planos de Ensino #
### Sistema criado sob encomenda para a Faculdade Católica do Tocantins. ###

## Tecnologias envolvidas: ##

* Maven
* Java - JDK 1.7+
* JSF - mojarra 2.2.7+
* Primefaces 5.1+
* Hibernate 4.3.10+
* JAAS
* JTA
* EJB 3.1
* CDI - Weld
* Log4J 2.3+
* Javacsv 2.*
* Wildfly 9.*
* MySQL 5.*

Em breve mais descrições sobre o sistema.