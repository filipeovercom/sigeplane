#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#USUARIOS
INSERT INTO sigeplane.Usuario (tipo, email, matricula, nome, perdeu_senha, perfil, senha) VALUES ('Discente','filipeovercom@gmail.com', 'FC20120897','Filipe D. Abreu',FALSE, 'ADMIN','Ujzu1mW8iE6BQYsBemA70S/SOtsXhIrBR151B9JxdZg=');
INSERT INTO sigeplane.Usuario (tipo, email, matricula, nome, perdeu_senha, perfil, senha) VALUES ('Discente','ranieu.sousa.9@gmail.com', 'FC20130311','Ranieu S. da Silva',FALSE, 'ADMIN','qwz3vEgudtxhbUIDtkuZabXRQHvPlx0M1RIMxIsa9+0=');
#SENHA DO USUARIO ADMIN E sige@123
INSERT INTO sigeplane.Usuario (tipo, email, matricula, nome, perdeu_senha, perfil, senha) VALUES ('Administrator','email@teste.com', 'admin','Administrador',FALSE, 'ADMIN','gpRNTN00ddgv4aIHMklVzT6yXLxuJAV4m140cLI6Cv4=');
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#CONFIGURACAO DE EMAIL PADRAO
INSERT INTO sigeplane.ConfigEnvioemail (autentica, contaAutenticacao, portServer, senhaAutenticacao, serverHost, usaSSL) VALUES (TRUE, 'sigeplane@gmail.com',465, 'sistema@sigeplane','smtp.gmail.com',TRUE);
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#Cursos
INSERT INTO sigeplane.Curso (codigo, descricao, nome)  VALUES ('1230', 'Curso teste da faculdade teste','Curso Teste');
INSERT INTO sigeplane.Curso (codigo, descricao, nome)  VALUES ('1231', 'Curso teste 2 da faculdade teste','Curso Teste 2');
INSERT INTO sigeplane.Curso (codigo, descricao, nome)  VALUES ('1232', 'Curso teste 3 da faculdade teste','Curso Teste 3');
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#Disciplinas
INSERT INTO sigeplane.Disciplina (cargaHorariaPratica, cargaHorariaTeorica, cargaHorariaTotal, codigo, ementa, nome, turma, curso_id, ativa)   VALUES (25,45,70,'1230','ementa da disciplina teste','Disciplina Teste','302N3A', 1, true);
INSERT INTO sigeplane.Disciplina (cargaHorariaPratica, cargaHorariaTeorica, cargaHorariaTotal, codigo, ementa, nome, turma, curso_id, ativa)   VALUES (25,45,70,'1231','ementa da disciplina teste 2','Disciplina Teste 2','301N4A', 2, true);
INSERT INTO sigeplane.Disciplina (cargaHorariaPratica, cargaHorariaTeorica, cargaHorariaTotal, codigo, ementa, nome, turma, curso_id, ativa)   VALUES (25,45,70,'1232','ementa da disciplina teste 3','Disciplina Teste 3','305N2A', 3, true);
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------