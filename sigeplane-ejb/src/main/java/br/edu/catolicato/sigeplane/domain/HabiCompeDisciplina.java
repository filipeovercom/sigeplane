/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.catolicato.sigeplane.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**---------------------------------------------------------
 * Criado por Filipe D. Abreu em 01 de Julho de 2015.      |
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino        |
 * Faculdade Católica do Tocantins - Sistemas de Informação|
 * ---------------------------------------------------------
 * Está entidade guarda as habilidades com suas devidas ava|
 * liações                                                         |
 *                                                         |
 *----------------------------------------------------------
 */
@Entity
class HabiCompeDisciplina implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @ManyToOne
    private Habilidade habilidade;
    
    @OneToMany
    private List<Conteudo> conteudos;

    @OneToMany
    private List<Metodologia> metodologias;

    @OneToMany
    private List<Recurso> recursos;

    public HabiCompeDisciplina() {
        this.conteudos = new LinkedList<>();
    }

    public HabiCompeDisciplina(Habilidade habilidade) {
        this.habilidade = habilidade;
    }

    public void addConteudo(Conteudo pConteudo){
        this.conteudos.add(pConteudo);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Habilidade getHabilidade() {
        return habilidade;
    }

    public void setHabilidade(Habilidade habilidade) {
        this.habilidade = habilidade;
    }

    public List<Conteudo> getConteudos() {
        return conteudos;
    }

    public void setConteudos(List<Conteudo> conteudos) {
        this.conteudos = conteudos;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HabiCompeDisciplina other = (HabiCompeDisciplina) obj;
        return Objects.equals(this.id, other.id);
    }
}