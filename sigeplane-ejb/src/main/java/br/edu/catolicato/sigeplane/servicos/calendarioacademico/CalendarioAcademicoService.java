package br.edu.catolicato.sigeplane.servicos.calendarioacademico;

import br.edu.catolicato.sigeplane.domain.CalendarioAcademico;
import br.edu.catolicato.sigeplane.domain.StatusCalendario;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.gestaoPlanoEnsino.PlanoEnsinoService;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 24 de Julho de 2015. SIGEPLANE - Sistema de
 * Gestão de Plano de Ensino Faculdade Católica do Tocantins - Sistemas de
 * Informação
 */
public class CalendarioAcademicoService implements Serializable {

    private static final long serialVersionUID = 1L;

    private Repositorio repositorio;
    @Inject
    private PlanoEnsinoService vrPlanoEnsinoService;

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
        this.vrPlanoEnsinoService.setRepositorio(repositorio);
    }

    /**
     * @param pCalendarioAcademico
     * @return int
     * <ul>
     * <li>0 - Erro, Calendário Acadêmico já existe</li>
     * <li>1 - Erro, Calendário Acadêmico Sem Aulas</li>
     * <li>2 - Erro, Ao Salvar Calendário Acadêmico</li>
     * <li>3 - OK, Calendário Acadêmico Salvo com Sucesso</li>
     * </ul>
     */
    public int salvarCalendario(CalendarioAcademico pCalendarioAcademico) {
        if (this.findCalendarioByReferencia(pCalendarioAcademico.getReferencia()) == null) {
            if (pCalendarioAcademico.getAulasCalendario().size() > 0) {
                if (repositorio.salvar(pCalendarioAcademico)) {
                    return 3;
                } else {
                    return 2;
                }
            } else {
                return 1;
            }
        }
        return 0;
    }

    public boolean editarCalendario(CalendarioAcademico pCalendarioAcademico) {
        return repositorio.editar(pCalendarioAcademico);
    }

    public boolean excluirCalendario(CalendarioAcademico pCalendarioAcademico) {
        return repositorio.excluir(pCalendarioAcademico);
    }

    public CalendarioAcademico buscaPorId(Integer id) {
        return repositorio.buscaPorId(CalendarioAcademico.class, id);
    }

    public List<CalendarioAcademico> buscaTodos() {
        return repositorio.buscaTodos(CalendarioAcademico.class);
    }

    public CalendarioAcademico findCalendarioByReferencia(String referencia) {
        return repositorio.executeQuerySingleResult(CalendarioAcademico.class,
                "select c from CalendarioAcademico c " +
                        "where c.referencia = ?1", referencia);
    }

    public CalendarioAcademico findCalendarioAtivo() {
        return repositorio.executeQuerySingleResult(CalendarioAcademico.class,
                "select c from CalendarioAcademico c " +
                        "where c.status = ?1", StatusCalendario.EM_ANDAMENTO);
    }

    public List<CalendarioAcademico> findAllCalendComHrDisciplinas() {
        return repositorio.executeQueryMultipleResult(CalendarioAcademico.class,
                "select c from CalendarioAcademico c " +
                        "where c.disciplinas.size > 0");
    }

    /**
     * @param calendAcadSelecionado CalendarioAcademico
     * @return <ul>
     * <li style="color: rgb(0,191,0);"> 1 | OK, Status atualizado com sucesso</li>
     * <li style="color: red;">-1 | Erro, Já existe um calendário em andamento</li>
     * <li style="color: red;">-2 | Erro, Não foi possível fechar os planos de ensino do calendário</li>
     * <li style="color: red;">-3 | Erro ao realizar operação</li>
     * <li style="color: red;">-4 | Erro ao reabrir planos de ensino</li>
     * </ul>
     */
    public int mudarStatusCalendario(CalendarioAcademico calendAcadSelecionado, StatusCalendario pNewStatus) {
        //TODO -> Quando o status do calendário for Iniciado e o novo status for Em andamento deve-se verificar se existe algum plano de ensino aguardando aprovação, caso exista deve-se abortar a operação e sinalizar usuário!
        if (pNewStatus.equals(StatusCalendario.EM_ANDAMENTO)) {
            if (findCalendarioAtivo() != null) {
                return -1;
            }
        } else if (pNewStatus.equals(StatusCalendario.FECHADO)) {
            if (!vrPlanoEnsinoService.fechaPlanosEnsino(calendAcadSelecionado)) {
                return -2;
            }
        }
        if (calendAcadSelecionado.getStatus().equals((StatusCalendario.FECHADO))) {
            if (!vrPlanoEnsinoService.reabrePlanosEnsino(calendAcadSelecionado)) {
                return -4;
            }
        }
        calendAcadSelecionado.setStatus(pNewStatus);
        if (!editarCalendario(calendAcadSelecionado)) {
            return -3;
        }
        return 1;
    }
}
