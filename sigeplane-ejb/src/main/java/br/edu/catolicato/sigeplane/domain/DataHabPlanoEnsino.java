package br.edu.catolicato.sigeplane.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Criado por Filipe D. Abreu em 01 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Entity
public class DataHabPlanoEnsino implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Temporal(TemporalType.DATE)
    private Date data;

    public DataHabPlanoEnsino() {
    }

    public DataHabPlanoEnsino(Date data) {
        this.data = data;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataHabPlanoEnsino that = (DataHabPlanoEnsino) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
