package br.edu.catolicato.sigeplane.util;

import br.edu.catolicato.sigeplane.domain.TipoArquivo;

import java.io.*;
import java.text.Normalizer;

/**
 * Criado por Filipe D. Abreu em 27 de Junho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Católica do Tocantins - Sistemas de Informação
 */
public class Utilidades {

    /**
     * Método retorna o caminho do diretório
     * onde ficam salvos os arquivos
     * @return String - diretorio
     * @since 1.0
     */
    public static String getMediaPath(TipoArquivo tipoArquivo) {
        if (tipoArquivo.equals(TipoArquivo.IMAGEM)) {
            return "e:"+ File.separator + "ArquivosSigeplane" + File.separator +"imagens"+ File.separator;

        } else if (tipoArquivo.equals(TipoArquivo.PDF)) {
            return "e:"+ File.separator + "ArquivosSigeplane" + File.separator +"pdf"+ File.separator;

        } else if (tipoArquivo.equals(TipoArquivo.EXCEL)) {
            return "e:"+ File.separator + "ArquivosSigeplane" + File.separator +"excel"+ File.separator;

        } else if (tipoArquivo.equals(TipoArquivo.CSV)) {
            return "e:"+ File.separator + "ArquivosSigeplane" + File.separator +"csv"+ File.separator;

        } else if (tipoArquivo.equals(TipoArquivo.WORD)) {
            return "e:"+ File.separator + "ArquivosSigeplane" + File.separator +"word"+ File.separator;

        } else if (tipoArquivo.equals(TipoArquivo.VIDEO)) {
            return "e:"+ File.separator + "ArquivosSigeplane" + File.separator +"video"+ File.separator;

        } else {
            return null;
        }
    }

    /**
     * Este metodo e responsavel por retirar os acentos de um string
     *
     * @param acentuada - String
     * @return string sem acentos
     * @since 1.0
     */
    public static String removerAcentos(String acentuada) {
        CharSequence cs = new StringBuilder(acentuada);
        String normalizada = Normalizer.normalize(cs, Normalizer.Form.NFKD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
        normalizada = normalizada.replace(" ", "_");
        normalizada = normalizada.replace(".", "");
        normalizada = normalizada.replace("/", "");
        normalizada = normalizada.replace("+", "");
        normalizada = normalizada.replace(":", "");
        normalizada = normalizada.replace("=", "");
        normalizada = normalizada.replace("ç", "c");
        normalizada = normalizada.replace("[", "_");
        normalizada = normalizada.replace("]", "_");
        normalizada = normalizada.toLowerCase();
        return normalizada;
    }

    public static byte[] fileToByte(File file) throws Exception {
        return Utilidades.fileToByte(new FileInputStream(file));
    }

    public static byte[] fileToByte(InputStream fis) throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        try {
            for (int readNum; (readNum = fis.read(buf)) != -1;) {
                bos.write(buf, 0, readNum);
            }
        } catch (IOException ex) {
        }
        return bos.toByteArray();
    }
}
