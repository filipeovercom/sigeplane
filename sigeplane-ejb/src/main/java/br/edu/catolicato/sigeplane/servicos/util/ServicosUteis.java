package br.edu.catolicato.sigeplane.servicos.util;

import br.edu.catolicato.sigeplane.domain.DiaSemana;
import br.edu.catolicato.sigeplane.domain.TipoAula;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Criado por Filipe D. Abreu em 28 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class ServicosUteis implements Serializable {

    private static final long serialVersionUID = 1L;

    public static DiaSemana getDiaSemanaByDate(Date data) {
        Calendar c = Calendar.getInstance();
        c.setTime(data);
        int dia = c.get(Calendar.DAY_OF_WEEK);
        DiaSemana ret = null;
        switch (dia) {
            case 2:
                ret = DiaSemana.SEGUNDA;
                break;
            case 3:
                ret = DiaSemana.TERCA;
                break;
            case 4:
                ret = DiaSemana.QUARTA;
                break;
            case 5:
                ret = DiaSemana.QUINTA;
                break;
            case 6:
                ret = DiaSemana.SEXTA;
                break;
            case 7:
                ret = DiaSemana.SABADO;
                break;
        }
        return ret;
    }

    public static TipoAula getTipoAulaByString(String tipoAula) {
        if (tipoAula.equalsIgnoreCase("A")) {
            return TipoAula.AULA_NORMAL;
        } else if (tipoAula.equalsIgnoreCase("R")) {
            return TipoAula.REPOSICAO;
        } else if (tipoAula.equalsIgnoreCase("S")) {
            return TipoAula.SUBSTITUTIVA;
        } else if (tipoAula.equalsIgnoreCase("E")) {
            return TipoAula.EXAME_FINAL;
        }
        return null;
    }

    public static DiaSemana getDiaSemanaByString(String diaAbrev) {
        if (diaAbrev.equalsIgnoreCase("SEG")) {
            return DiaSemana.SEGUNDA;
        } else if (diaAbrev.equalsIgnoreCase("TER")) {
            return DiaSemana.TERCA;
        } else if (diaAbrev.equalsIgnoreCase("QUA")) {
            return DiaSemana.QUARTA;
        } else if (diaAbrev.equalsIgnoreCase("QUI")) {
            return DiaSemana.QUINTA;
        } else if (diaAbrev.equalsIgnoreCase("SEX")) {
            return DiaSemana.SEXTA;
        } else if (diaAbrev.equalsIgnoreCase("SAB")) {
            return DiaSemana.SABADO;
        }
        return null;
    }
}
