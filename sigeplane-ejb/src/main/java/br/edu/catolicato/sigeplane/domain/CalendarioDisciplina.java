package br.edu.catolicato.sigeplane.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Criado por Filipe D. Abreu em 24 de Julho de 2015. SIGEPLANE - Sistema de
 * Gestão de Plano de Ensino Faculdade Católica do Tocantins - Sistemas de
 * Informação
 */
@Entity
public class CalendarioDisciplina implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private Disciplina disciplina;

    @ManyToOne
    private CalendarioAcademico calendarioAcademico;

    @Enumerated(EnumType.STRING)
    private DiaSemana diaSemana;

    public CalendarioDisciplina() {
    }

    public CalendarioDisciplina(Disciplina disciplina, CalendarioAcademico calendarioAcademico, DiaSemana diaSemana) {
        this.disciplina = disciplina;
        this.calendarioAcademico = calendarioAcademico;
        this.diaSemana = diaSemana;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    public CalendarioAcademico getCalendarioAcademico() {
        return calendarioAcademico;
    }

    public void setCalendarioAcademico(CalendarioAcademico calendarioAcademico) {
        this.calendarioAcademico = calendarioAcademico;
    }

    public DiaSemana getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(DiaSemana diaSemana) {
        this.diaSemana = diaSemana;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.disciplina);
        hash = 11 * hash + Objects.hashCode(this.calendarioAcademico);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CalendarioDisciplina other = (CalendarioDisciplina) obj;
        if (!Objects.equals(this.disciplina, other.disciplina)) {
            return false;
        }
        return Objects.equals(this.calendarioAcademico, other.calendarioAcademico);
    }
}
