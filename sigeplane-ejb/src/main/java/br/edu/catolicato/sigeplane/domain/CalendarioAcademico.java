package br.edu.catolicato.sigeplane.domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Criado por Filipe D. Abreu em 01 de Julho de 2015. SIGEPLANE - Sistema de
 * Gestão de Plano de Ensino Faculdade Católica do Tocantins - Sistemas de
 * Informação
 */
@Entity
public class CalendarioAcademico implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "Semestre é obrigatório!")
    @Column(nullable = false)
    private String referencia;

    @Enumerated(EnumType.STRING)
    private StatusCalendario status;

    @NotEmpty(message = "Deve existir no mínimo 1 aula!")
    @OneToMany(mappedBy = "calendarioAcademico", cascade = CascadeType.ALL, orphanRemoval = true, targetEntity = AulaCalendario.class)
    private List<AulaCalendario> aulasCalendario;

    @OneToMany(mappedBy = "calendarioAcademico", cascade = CascadeType.ALL, orphanRemoval = true, targetEntity = CalendarioDisciplina.class)
    private List<CalendarioDisciplina> disciplinas;

    @OneToMany(mappedBy = "calendarioAcademico", cascade = CascadeType.ALL, orphanRemoval = true, targetEntity = CalendarioDocente.class)
    private List<CalendarioDocente> docentes;

    @OneToMany(mappedBy = "calendarioAcademico", cascade = CascadeType.ALL, orphanRemoval = true, targetEntity = CalendarioDiscente.class)
    private List<CalendarioDiscente> discentes;

    @OneToMany(mappedBy = "calendarioAcademico", cascade = CascadeType.ALL, orphanRemoval = true, targetEntity = PlanoEnsino.class)
    private List<PlanoEnsino> planosEnsino;

    public CalendarioAcademico() {
        this.aulasCalendario = new LinkedList<>();
        this.disciplinas = new LinkedList<>();
        this.docentes = new LinkedList<>();
        this.discentes = new LinkedList<>();
        this.planosEnsino = new LinkedList<>();
    }

    public CalendarioAcademico(String referencia, StatusCalendario status) {
        this.referencia = referencia;
        this.status = status;
        this.aulasCalendario = new LinkedList<>();
        this.disciplinas = new LinkedList<>();
        this.docentes = new LinkedList<>();
        this.discentes = new LinkedList<>();
        this.planosEnsino = new LinkedList<>();
    }

    public void addAulaCalendario(AulaCalendario pAulaCalendario) {
        this.aulasCalendario.add(pAulaCalendario);
        pAulaCalendario.setCalendarioAcademico(this);
    }

    public void addCalendDisciplina(CalendarioDisciplina pCalendarioDisciplina) {
        this.disciplinas.add(pCalendarioDisciplina);
        pCalendarioDisciplina.setCalendarioAcademico(this);
    }

    public void addPlanoEnsino(PlanoEnsino pPlanoEnsino) {
        this.planosEnsino.add(pPlanoEnsino);
        pPlanoEnsino.setCalendarioAcademico(this);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public StatusCalendario getStatus() {
        return status;
    }

    public void setStatus(StatusCalendario status) {
        this.status = status;
    }

    public List<AulaCalendario> getAulasCalendario() {
        return aulasCalendario;
    }

    public void setAulasCalendario(List<AulaCalendario> aulaCalendarios) {
        this.aulasCalendario = aulaCalendarios;
    }

    public List<CalendarioDisciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<CalendarioDisciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public List<CalendarioDocente> getDocentes() {
        return docentes;
    }

    public void setDocentes(List<CalendarioDocente> docentes) {
        this.docentes = docentes;
    }

    public List<CalendarioDiscente> getDiscentes() {
        return discentes;
    }

    public void setDiscentes(List<CalendarioDiscente> discentes) {
        this.discentes = discentes;
    }

    public List<PlanoEnsino> getPlanosEnsino() {
        return planosEnsino;
    }

    public void setPlanosEnsino(List<PlanoEnsino> planosEnsino) {
        this.planosEnsino = planosEnsino;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CalendarioAcademico that = (CalendarioAcademico) o;
        return Objects.equals(getReferencia(), that.getReferencia());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getReferencia());
    }
}
