package br.edu.catolicato.sigeplane.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Criado por Filipe D. Abreu em 27 de Junho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Católica do Tocantins - Sistemas de Informação
 */
@Entity
public class Titulacao implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private NivelFormacao nivelFormacao;

    @ManyToOne(cascade = CascadeType.ALL)
    private Formacao formacao;

    public Titulacao() {}

    public Titulacao(NivelFormacao nivelFormacao, Formacao formacao) {
        this.nivelFormacao = nivelFormacao;
        this.formacao = formacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public NivelFormacao getNivelFormacao() {
        return nivelFormacao;
    }

    public void setNivelFormacao(NivelFormacao nivelFormacao) {
        this.nivelFormacao = nivelFormacao;
    }

    public Formacao getFormacao() {
        return formacao;
    }

    public void setFormacao(Formacao formacao) {
        this.formacao = formacao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Titulacao titulacao = (Titulacao) o;
        return Objects.equals(getId(), titulacao.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
