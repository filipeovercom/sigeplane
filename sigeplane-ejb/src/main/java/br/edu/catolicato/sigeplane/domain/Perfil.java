package br.edu.catolicato.sigeplane.domain;

/**
 * Criado por Filipe D. Abreu em 27 de Junho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Católica do Tocantins - Sistemas de Informação
 */
public enum  Perfil {

DISCENTE("DISCENTE"), DOCENTE("DOCENTE"), NDE("NDE"), ADMINISTRADOR("ADMINISTRADOR"), ADMIN("ADMIN");

    private String descricao;

    Perfil(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
