package br.edu.catolicato.sigeplane.servicos.gestaoUsuarios;

import br.edu.catolicato.sigeplane.domain.Docente;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;

import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 09 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class DocenteService implements Serializable {
    private static final long serialVersionUID = 1L;

    private Repositorio repositorio;

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    public boolean salvarDocente(Docente pDocente) {
        return repositorio.salvar(pDocente);
    }

    public boolean salvarDocentes(List<Docente> pDocentes) {
        return repositorio.salvarLista(pDocentes);
    }

    public boolean editarDocente(Docente pDocente) {
        return repositorio.editar(pDocente);
    }

    public boolean editarDocentes(List<Docente> pDocentes) {
        return repositorio.editarLista(pDocentes);
    }

    public boolean excluirDocente(Docente pDocente) {
        return repositorio.excluir(pDocente);
    }

    public List<Docente> buscaTodos() {
        return repositorio.buscaTodos(Docente.class);
    }

    public Docente buscaPorId(Integer id) {
        return repositorio.buscaPorId(Docente.class, id);
    }

    public Docente findDocenteByMatricula(String matricula) {
        return repositorio.executeQuerySingleResult(Docente.class, "select d from Docente d " +
                "where d.matricula = ?1", matricula);
    }
}
