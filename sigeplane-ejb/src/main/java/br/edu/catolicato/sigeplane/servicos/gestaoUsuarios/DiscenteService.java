package br.edu.catolicato.sigeplane.servicos.gestaoUsuarios;

import br.edu.catolicato.sigeplane.domain.Discente;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;

import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 09 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class DiscenteService implements Serializable {
    private static final long serialVersionUID = 1L;

    private Repositorio repositorio;

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    public boolean salvarDiscente(Discente pDiscente) {
        return repositorio.salvar(pDiscente);
    }

    public boolean salvarDiscentes(List<Discente> pDiscentes) {
        return repositorio.salvarLista(pDiscentes);
    }

    public boolean editarDiscente(Discente pDiscente) {
        return repositorio.editar(pDiscente);
    }

    public boolean editarDiscentes(List<Discente> pDiscentes) {
        return repositorio.editarLista(pDiscentes);
    }

    public boolean excluirDiscente(Discente pDiscente) {
        return repositorio.excluir(pDiscente);
    }

    public List<Discente> buscaTodos() {
        return repositorio.buscaTodos(Discente.class);
    }

    public Discente buscaPorId(Integer id) {
        return repositorio.buscaPorId(Discente.class, id);
    }

    public Discente findDiscenteByMatricula(String matricula) {
        return repositorio.executeQuerySingleResult(Discente.class, "select d from Discente d " +
                "where d.matricula = ?1", matricula);
    }
}
