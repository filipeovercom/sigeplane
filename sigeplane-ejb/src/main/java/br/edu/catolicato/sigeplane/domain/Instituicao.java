package br.edu.catolicato.sigeplane.domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Criado por Filipe D. Abreu em 27 de Junho de 2015. SIGEPLANE - Sistema de
 * Gestão de Plano de Ensino Católica do Tocantins - Sistemas de Informação
 */
@Entity
public class Instituicao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Column(nullable = false, unique = true)
    private String nome;

    @OneToOne
    private Arquivo logotipo;

    private String missao;

    private String visao;

    @OneToMany(mappedBy = "instituicao")
    private List<Campus> listCampus;

    public Instituicao() {
        this.listCampus = new LinkedList<>();
    }

    public Instituicao(String nome, Arquivo logotipo, String missao, String visao) {
        this.nome = nome;
        this.logotipo = logotipo;
        this.missao = missao;
        this.visao = visao;
        this.listCampus = new LinkedList<>();
    }

    public void adicionaCampus(Campus campus) {
        this.listCampus.add(campus);
        campus.setInstituicao(this);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Arquivo getLogotipo() {
        return logotipo;
    }

    public void setLogotipo(Arquivo logotipo) {
        this.logotipo = logotipo;
    }

    public String getMissao() {
        return missao;
    }

    public void setMissao(String missao) {
        this.missao = missao;
    }

    public String getVisao() {
        return visao;
    }

    public void setVisao(String visao) {
        this.visao = visao;
    }

    public List<Campus> getListCampus() {
        return listCampus;
    }

    public void setListCampus(List<Campus> listCampus) {
        this.listCampus = listCampus;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.nome);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Instituicao other = (Instituicao) obj;
        return Objects.equals(this.nome, other.nome);
    }

}
