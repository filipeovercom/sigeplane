package br.edu.catolicato.sigeplane.servicos.envioEmail;

import br.edu.catolicato.sigeplane.domain.ConfigEnvioEmail;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import com.outjected.email.api.SessionConfig;
import com.outjected.email.impl.SimpleMailConfig;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 22 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class ConfigEnvioEmailService implements Serializable {

    private static final long serialVersionUID = 1L;

    private Repositorio repositorio;

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    @Produces
    @RequestScoped
    public SessionConfig getSessaoDeConfiguracao() {
        ConfigEnvioEmail configuracaoSalva = this.getConfiguracao();
        SimpleMailConfig simpleMailConf = new SimpleMailConfig();
        simpleMailConf.setServerHost(configuracaoSalva.getServerHost());
        simpleMailConf.setServerPort(configuracaoSalva.getPortServer());
        simpleMailConf.setAuth(configuracaoSalva.isAutentica());
        simpleMailConf.setEnableSsl(configuracaoSalva.isUsaSSL());
        simpleMailConf.setUsername(configuracaoSalva.getContaAutenticacao());
        simpleMailConf.setPassword(configuracaoSalva.getSenhaAutenticacao());
        return simpleMailConf;
    }

    public boolean salvarConfiguracao(ConfigEnvioEmail pConfigEnvioEmail) {
        return repositorio.salvar(pConfigEnvioEmail);
    }

    public boolean editarConfiguracao(ConfigEnvioEmail pConfigEnvioEmail) {
        return repositorio.editar(pConfigEnvioEmail);
    }

    public boolean excluirConfiguracao(ConfigEnvioEmail pConfigEnvioEmail) {
        return repositorio.excluir(pConfigEnvioEmail);
    }

    public ConfigEnvioEmail getConfiguracao() {
        List<ConfigEnvioEmail> lista = repositorio.buscaTodos(ConfigEnvioEmail.class);
        if (lista != null && lista.size() > 0) {
            return lista.get(0);
        }
        return new ConfigEnvioEmail();
    }
}
