package br.edu.catolicato.sigeplane.domain;

/**
 * Criado por Filipe D. Abreu em 02 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public enum DiaSemana {
    SEGUNDA("Segunda-Feira"), TERCA("Terça-Feira"), QUARTA("Quarta-Feira"), QUINTA("Quinta-Feira"), SEXTA("Sexta-Feira"), SABADO("Sábado");

    private String descricao;

    DiaSemana(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
