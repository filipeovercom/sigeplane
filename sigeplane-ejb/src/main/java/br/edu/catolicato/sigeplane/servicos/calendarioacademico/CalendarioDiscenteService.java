package br.edu.catolicato.sigeplane.servicos.calendarioacademico;

import br.edu.catolicato.sigeplane.domain.CalendarioAcademico;
import br.edu.catolicato.sigeplane.domain.CalendarioDiscente;
import br.edu.catolicato.sigeplane.domain.Discente;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;

import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 21 de Agosto de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class CalendarioDiscenteService implements Serializable {
    private static final long serialVersionUID = 1L;

    private Repositorio repositorio;

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    /*TRANSAÇÕES BÁSICAS*/
    public boolean salvarHorario(CalendarioDiscente pCalendarioDiscente) {
        return repositorio.salvar(pCalendarioDiscente);
    }

    public boolean salvarHorarios(List<CalendarioDiscente> listaHorario) {
        return repositorio.salvarLista(listaHorario);
    }

    public boolean editarHorario(CalendarioDiscente pCalendarioDiscente) {
        return repositorio.editar(pCalendarioDiscente);
    }

    public boolean excluirHorario(CalendarioDiscente pCalendarioDiscente) {
        return repositorio.excluir(pCalendarioDiscente);
    }

    /*BUSCAS*/
    public List<CalendarioDiscente> buscaTodos() {
        return repositorio.buscaTodos(CalendarioDiscente.class);
    }

    public CalendarioDiscente findHrByDiscenteCalendario(Discente pDiscente, CalendarioAcademico calendarioAcademico) {
        return repositorio.executeQuerySingleResult(CalendarioDiscente.class,
                "SELECT h from CalendarioDiscente h " +
                        "where h.discente = ?1 and h.calendarioAcademico = ?2", pDiscente, calendarioAcademico);
    }

    public CalendarioDiscente buscaPorId(Integer id) {
        return repositorio.buscaPorId(CalendarioDiscente.class, id);
    }
}
