package br.edu.catolicato.sigeplane.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Criado por Filipe D. Abreu em 22 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Entity
public class ConfigEnvioEmail implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false)
    private String serverHost;
    @Column(nullable = false)
    private Integer portServer;
    @Column(nullable = false)
    private boolean autentica;
    @Column(nullable = false)
    private boolean usaSSL;
    private String contaAutenticacao;
    private String senhaAutenticacao;

    public ConfigEnvioEmail() {
    }

    public ConfigEnvioEmail(String serverHost, Integer portServer, boolean autentica,
                            boolean usaSSL, String contaAutenticacao, String senhaAutenticacao) {
        this.serverHost = serverHost;
        this.portServer = portServer;
        this.autentica = autentica;
        this.usaSSL = usaSSL;
        this.contaAutenticacao = contaAutenticacao;
        this.senhaAutenticacao = senhaAutenticacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getServerHost() {
        return serverHost;
    }

    public void setServerHost(String serverHost) {
        this.serverHost = serverHost;
    }

    public Integer getPortServer() {
        return portServer;
    }

    public void setPortServer(Integer portServer) {
        this.portServer = portServer;
    }

    public boolean isAutentica() {
        return autentica;
    }

    public void setAutentica(boolean autentica) {
        this.autentica = autentica;
    }

    public boolean isUsaSSL() {
        return usaSSL;
    }

    public void setUsaSSL(boolean ssl) {
        this.usaSSL = ssl;
    }

    public String getContaAutenticacao() {
        return contaAutenticacao;
    }

    public void setContaAutenticacao(String contaAutenticacao) {
        this.contaAutenticacao = contaAutenticacao;
    }

    public String getSenhaAutenticacao() {
        return senhaAutenticacao;
    }

    public void setSenhaAutenticacao(String senhaAutenticacao) {
        this.senhaAutenticacao = senhaAutenticacao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConfigEnvioEmail that = (ConfigEnvioEmail) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
