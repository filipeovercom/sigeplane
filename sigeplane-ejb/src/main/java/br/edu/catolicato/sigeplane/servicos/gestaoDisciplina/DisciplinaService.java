package br.edu.catolicato.sigeplane.servicos.gestaoDisciplina;

import br.edu.catolicato.sigeplane.domain.Disciplina;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;

import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 17 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class DisciplinaService implements Serializable {
    private static final long serialVersionUID = 1L;

    private Repositorio repositorio;

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    /*Transacoes*/
    public boolean salvar(Disciplina disciplina) {
        return repositorio.salvar(disciplina);
    }

    public boolean editar(Disciplina disciplina) {
        return repositorio.editar(disciplina);
    }

    public boolean excluir(Disciplina disciplina) {
        return repositorio.excluir(disciplina);
    }

    /*Buscas*/
    public List<Disciplina> buscaTodos() {
        return repositorio.buscaTodos(Disciplina.class);
    }

    public List<Disciplina> findDisciplinaByNome(String nome) {
        return repositorio.executeQueryMultipleResult(Disciplina.class, "select d from Disciplina d " +
                "where d.nome = ?1 and d.ativa = true", nome);
    }

    public Disciplina findDisciplinaByCodigo(String codigo) {
        return repositorio.executeQuerySingleResult(Disciplina.class, "select d from Disciplina d " +
                "where d.codigo = ?1 and d.ativa = true", codigo);
    }

    public Disciplina buscaPorId(Integer id) {
        return repositorio.buscaPorId(Disciplina.class, id);
    }

    public boolean salvarDisciplinas(List<Disciplina> pDisciplinas) {
        return repositorio.salvarLista(pDisciplinas);
    }

    public boolean editarDisciplinas(List<Disciplina> pDisciplinas) {
        return repositorio.editarLista(pDisciplinas);
    }

}
