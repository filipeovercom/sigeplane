/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.catolicato.sigeplane.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author filipe
 */
@Entity
public class CalendarioDocente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message = "Calendário Acadêmico é obrigatório!")
    @ManyToOne
    private CalendarioAcademico calendarioAcademico;

    @NotNull(message = "Docente é obrigatório!")
    @ManyToOne
    private Docente docente;

    @ManyToMany
    private List<Disciplina> disciplinas;

    public CalendarioDocente() {
        this.disciplinas = new LinkedList<>();
    }

    public CalendarioDocente(CalendarioAcademico calendarioAcademico, Docente docente) {
        this.calendarioAcademico = calendarioAcademico;
        this.docente = docente;
        this.disciplinas = new LinkedList<>();
    }

    public void addDisciplina(Disciplina pDisciplina) {
        this.disciplinas.add(pDisciplina);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CalendarioAcademico getCalendarioAcademico() {
        return calendarioAcademico;
    }

    public void setCalendarioAcademico(CalendarioAcademico semestre) {
        this.calendarioAcademico = semestre;
    }

    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    public List<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CalendarioDocente that = (CalendarioDocente) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
