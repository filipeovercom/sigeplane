package br.edu.catolicato.sigeplane.persistencia;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 28 de Junho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Stateful
public class Repositorio implements Serializable {

    private static final long serialVersionUID = 1L;
    private Logger log;

    @PersistenceContext(unitName = "sigeplanePU", type = PersistenceContextType.EXTENDED)
    private EntityManager entityManager;

    @PostConstruct
    public void init() {
        this.log = LogManager.getLogger(Repositorio.class);
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void setLog(Logger log) {
        this.log = log;
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public boolean salvar(Object entity) {
        try {
            entityManager.persist(entity);
            return true;
        } catch (Exception ex) {
            log.error("Erro ao Salvar objeto: " + entity.toString());
            log.error("Mensagem do erro: " + ex.getMessage());
            if (ex.getCause() != null) {
                log.error("Causa do Erro: " + ex.getCause().getMessage());
            }
            return false;
        }
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public boolean editar(Object entity) {
        try {
            entityManager.merge(entity);
            return true;
        } catch (Exception ex) {
            log.error("Erro ao Editar Objeto: " + entity.toString());
            log.error("Mensagem do erro: " + ex.getMessage());
            if (ex.getCause() != null) {
                log.error("Causa do erro: " + ex.getCause().getMessage());
            }
            ex.printStackTrace();
            return false;
        }
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public boolean excluir(Object entity) {
        try {
            if (entityManager.contains(entity)) {
                entityManager.remove(entity);
            } else {
                entityManager.remove(entityManager.merge(entity));
            }
            return true;
        } catch (Exception ex) {
            log.error("Erro ao Excluir objeto: " + entity.toString());
            log.error("Mensagem do erro: " + ex.getMessage());
            if (ex.getCause() != null) {
                log.error("Causa do erro " + ex.getCause().getMessage());
            }
            return false;
        }
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public boolean salvarLista(List pLista) {
        /*Inicia Transação*/
        int cont = 0;
        for (Object entity : pLista) {
            try {
                entityManager.persist(entity);
                if (cont++ % 50 == 0) {
                    entityManager.flush();
                    entityManager.clear();
                }
            } catch (Exception ex) {
                log.error("Erro ao Salvar objeto: " + entity.toString());
                log.error("Mensagem do erro: " + ex.getMessage());
                if (ex.getCause() != null) {
                    log.error("Causa do Erro: " + ex.getCause().getMessage());
                }
                return false;
            }
        }
        /*Fim da Transação*/
        return true;
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public boolean editarLista(List pLista) {
        /*Inicia Transação*/
        int cont = 0;
        for (Object entity : pLista) {
            try {
                entityManager.merge(entity);
                if (cont++ % 50 == 0) {
                    entityManager.flush();
                    entityManager.clear();
                }
            } catch (Exception ex) {
                log.error("Erro ao Salvar objeto: " + entity.toString());
                log.error("Mensagem do erro: " + ex.getMessage());
                if (ex.getCause() != null) {
                    log.error("Causa do Erro: " + ex.getCause().getMessage());
                }
                return false;
            }
        }
        /*Fim da Transação*/

        return true;
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public boolean excluirLista(List pLista) {
        /*Inicia Transação*/
        int cont = 1;
        for (Object entity : pLista) {
            try {
                if (entityManager.contains(entity)) {
                    entityManager.remove(entity);
                } else {
                    entityManager.remove(entityManager.merge(entity));
                }
                if (cont++ % 50 == 0) {
                    entityManager.flush();
                    entityManager.clear();
                }
            } catch (Exception ex) {
                log.error("Erro ao Salvar objeto: " + entity.toString());
                log.error("Mensagem do erro: " + ex.getMessage());
                if (ex.getCause() != null) {
                    log.error("Causa do Erro: " + ex.getCause().getMessage());
                }
                return false;
            }
        }
        /*Fim da Transação*/

        return true;
    }

    @Transactional(Transactional.TxType.SUPPORTS)
    public <T> List<T> buscaTodos(Class<T> clasToCast) {
        try {
            return entityManager.createQuery("FROM " + clasToCast.getName() + " t ORDER BY t.id", clasToCast).getResultList();
        } catch (NoResultException ex) {
            log.debug("Nenhum resgistro encontrado na consulta!");
            return null;
        }
    }

    @Transactional(Transactional.TxType.SUPPORTS)
    public <T> T buscaPorId(Class<T> clasToCast, Integer pId) {
        try {
            return (T) entityManager.createQuery("FROM " + clasToCast.getName() + " t WHERE t.id = :id", clasToCast).
                    setParameter("id", pId).getSingleResult();
        } catch (NoResultException ex) {
            log.debug("Nenhum resgistro encontrado na consulta!");
            return null;
        }
    }

    @Transactional(Transactional.TxType.SUPPORTS)
    public <T> T executeQuerySingleResult(Class<T> clasToCast, String query, Object... parameters) {
        try {
            Query q = entityManager.createQuery(query, clasToCast);
            for (int i = 0; i < parameters.length; i++) {
                q.setParameter(i + 1, parameters[i]);
            }
            return (T) q.getSingleResult();
        } catch (NoResultException ex) {
            log.debug("Nenhum resgistro encontrado na consulta - executeQuerySingleResult!");
            return null;
        }
    }

    @Transactional(Transactional.TxType.SUPPORTS)
    public <T> List<T> executeQueryMultipleResult(Class<T> clasToCast, String query, Object... parameters) {
        try {
            Query q = entityManager.createQuery(query, clasToCast);
            for (int i = 0; i < parameters.length; i++) {
                q.setParameter(i + 1, parameters[i]);
            }
            return q.getResultList();
        } catch (Exception ex) {
            log.error("Erro executar query: " + query);
            if (ex.getCause() != null) {
                log.error("Causa: " + ex.getCause().getMessage());
            } else {
                log.error("Erro informado: " + ex);
            }
            return null;
        }
    }

    @Transactional(Transactional.TxType.SUPPORTS)
    public <T> List<T> executeNativeQueryMultipleResult(Class<T> clasToCast, String query, Object... parameters) {
        try {
            Query q = entityManager.createNativeQuery(query, clasToCast);
            for (int i = 0; i < parameters.length; i++) {
                q.setParameter(i + 1, parameters[i]);
            }
            return q.getResultList();
        } catch (Exception ex) {
            log.error("Erro executar query: " + query);
            if (ex.getCause() != null) {
                log.error("Causa: " + ex.getCause().getMessage());
            } else {
                log.error("Erro informado: " + ex);
            }
            return null;
        }
    }
}
