package br.edu.catolicato.sigeplane.servicos.gestaoPlanoEnsino;

import br.edu.catolicato.sigeplane.domain.*;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioDisciplinaService;
import br.edu.catolicato.sigeplane.servicos.calendarioacademico.CalendarioDocenteService;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 27 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class PlanoEnsinoService implements Serializable {
    private static final long serialVersionUID = 1L;

    private Repositorio repositorio;
    @Inject
    private CalendarioDisciplinaService calendarioDisciplinaService;
    @Inject
    private CalendarioDocenteService calendarioDocenteService;

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    /**
     * <h1>Método responsável por criar os planos de ensino para as disciplinas do calendário acadêmico ativo.</h1>
     *
     * @return <ul>
     * <li>true | OK, todos os planos de ensino foram criados</li>
     * <li style="color: red;">false | Erro ao realizar a operação</li>
     * </ul>
     */
    public boolean iniciarPlanosEnsino(CalendarioAcademico pCalendarioAcademico) {
        try {
            List<Disciplina> vrDisciplinas = calendarioDisciplinaService.findDisciplinasWithCalendario(pCalendarioAcademico);
            for (Disciplina vrDisciplina : vrDisciplinas) {
                Docente vrDocente = calendarioDocenteService.findProfComDiscEmCalend(pCalendarioAcademico, vrDisciplina);
                PlanoEnsino vrNovoPlanoEnsino = new PlanoEnsino();
                if (vrDisciplina.getPlanosEnsino().size() > 0) {
                    PlanoEnsino p = vrDisciplina.getPlanosEnsino().get(vrDisciplina.getPlanosEnsino().size() - 1);
                    vrNovoPlanoEnsino.setBibliografiaBasica(p.getBibliografiaBasica());
                    vrNovoPlanoEnsino.setBibliografiaComplementar(p.getBibliografiaComplementar());
                    for (CompetenciaPlano cp : p.getCompetencias()) {
                        vrNovoPlanoEnsino.getCompetencias().add(new CompetenciaPlano(cp.getCompetencia(), vrNovoPlanoEnsino));
                    }
                    vrNovoPlanoEnsino.setCompetencias(p.getCompetencias());
                    vrNovoPlanoEnsino.setGuardaNumExibicoes(p.isGuardaNumExibicoes());
                    vrNovoPlanoEnsino.setStatus(StatusPlanoEnsino.ABERTO);
                } else {
                    vrNovoPlanoEnsino.setDisciplina(vrDisciplina);
                    vrNovoPlanoEnsino.setStatus(StatusPlanoEnsino.ABERTO);
                    vrNovoPlanoEnsino.setGuardaNumExibicoes(false);
                }
                vrDisciplina.addPlanoEnsino(vrNovoPlanoEnsino);
                pCalendarioAcademico.addPlanoEnsino(vrNovoPlanoEnsino);
                vrDocente.addPlanoEnsino(vrNovoPlanoEnsino);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * @param pCalendarioAcademico
     * @return true caso a operação seja completada com sucesso e false caso contrário.
     */
    public boolean fechaPlanosEnsino(CalendarioAcademico pCalendarioAcademico) {
        try {
            List<PlanoEnsino> planos = this.planosEnsinoByCalend(pCalendarioAcademico);
            for (PlanoEnsino p : planos) {
                p.setStatus(StatusPlanoEnsino.FECHADO);
            }
            return repositorio.editarLista(planos);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public StatusPlanoEnsino statusDoPlanoByDisciplinaAndCalend(Disciplina disciplina, CalendarioAcademico calendarioAcademico) {
        return repositorio.executeQuerySingleResult(StatusPlanoEnsino.class,
                "select p.status from PlanoEnsino p " +
                        "where p.disciplina = ?1 and p.calendarioAcademico = ?2", disciplina, calendarioAcademico);
    }

    public PlanoEnsino planoEnsinoByDisciplinaAndCalend(Disciplina disciplina, CalendarioAcademico calendarioAcademico) {
        return repositorio.executeQuerySingleResult(PlanoEnsino.class,
                "select p from PlanoEnsino p " +
                        "where p.disciplina = ?1 and p.calendarioAcademico = ?2", disciplina, calendarioAcademico);
    }

    public List<PlanoEnsino> planosEnsinoByCalend(CalendarioAcademico pCalendarioAcademico) {
        return repositorio.executeQueryMultipleResult(PlanoEnsino.class,
                "Select p from PlanoEnsino p where p.calendarioAcademico = ?1", pCalendarioAcademico);
    }

    public boolean reabrePlanosEnsino(CalendarioAcademico pCalendarioAcademico) {
        try {
            List<PlanoEnsino> planos = this.planosEnsinoByCalend(pCalendarioAcademico);
            for (PlanoEnsino p : planos) {
                p.setStatus(StatusPlanoEnsino.ABERTO);
            }
            return repositorio.editarLista(planos);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
