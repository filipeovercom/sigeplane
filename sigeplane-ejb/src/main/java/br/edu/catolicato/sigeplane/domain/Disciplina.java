package br.edu.catolicato.sigeplane.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Criado por Filipe D. Abreu em 27 de Junho de 2015. SIGEPLANE - Sistema de
 * Gestão de Plano de Ensino Católica do Tocantins - Sistemas de Informação
 */
@Entity
public class Disciplina implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "Código da Disciplina é obrigatório!")
    @Column(nullable = false, unique = true)
    private String codigo;

    @NotBlank(message = "Nome da Disciplina é obrigatório!")
    @Column(nullable = false)
    private String nome;

    private String ementa;

    @NotNull(message = "Curso da disciplina é obrigatório!")
    @JoinColumn(nullable = false)
    @ManyToOne
    @Fetch(FetchMode.JOIN)
    private Curso curso;

    @Range(min = 0, message = "Carga horária teórica deve ter no mínio 1")
    private Integer cargaHorariaTeorica;

    @Range(min = 0, message = "Carga horária prática deve ter no mínio 1")
    private Integer cargaHorariaPratica;

    @Range(min = 0, message = "Carga horária total deve ter no mínio 1")
    private Integer cargaHorariaTotal;

    private String turma;

    private boolean ativa;

    @OneToMany(mappedBy = "disciplina", cascade = {CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST},
            orphanRemoval = true, targetEntity = PlanoEnsino.class)
    private List<PlanoEnsino> planosEnsino;

    @OneToMany(mappedBy = "disciplina", cascade = CascadeType.ALL, orphanRemoval = true,
            targetEntity = CompetenciaDisciplina.class)
    private List<CompetenciaDisciplina> competenciasDisciplina;

    @OneToMany(mappedBy = "disciplina", orphanRemoval = true, targetEntity = CalendarioDisciplina.class)
    private List<CalendarioDisciplina> horarios;

    @ManyToMany(mappedBy = "disciplinas", cascade = {CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST},
            targetEntity = CalendarioDocente.class)
    private List<CalendarioDocente> horariosDocentes;

    @ManyToMany(mappedBy = "disciplinas", cascade = {CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST},
            targetEntity = CalendarioDiscente.class)
    private List<CalendarioDiscente> horariosDiscentes;

    public Disciplina() {
        this.competenciasDisciplina = new LinkedList<>();
        this.planosEnsino = new LinkedList<>();
        this.horarios = new LinkedList<>();
        this.horariosDocentes = new LinkedList<>();
        this.horariosDiscentes = new LinkedList<>();
    }

    public Disciplina(String codigo, String nome, String ementa, Curso curso, Integer cargaHorariaTeorica,
                      Integer cargaHorariaPratica, Integer cargaHorariaTotal, String turma) {
        this.codigo = codigo;
        this.nome = nome;
        this.ementa = ementa;
        this.curso = curso;
        this.cargaHorariaTeorica = cargaHorariaTeorica;
        this.cargaHorariaPratica = cargaHorariaPratica;
        this.cargaHorariaTotal = cargaHorariaTotal;
        this.turma = turma;
        this.planosEnsino = new LinkedList<>();
        this.competenciasDisciplina = new LinkedList<>();
        this.horarios = new LinkedList<>();
        this.horariosDocentes = new LinkedList<>();
        this.horariosDiscentes = new LinkedList<>();
    }

    public void addPlanoEnsino(PlanoEnsino pPlanoEnsino) {
        this.planosEnsino.add(pPlanoEnsino);
        pPlanoEnsino.setDisciplina(this);
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public List<CompetenciaDisciplina> getCompetenciasDisciplina() {
        return competenciasDisciplina;
    }

    public void setCompetenciasDisciplina(List<CompetenciaDisciplina> competenciasDisciplina) {
        this.competenciasDisciplina = competenciasDisciplina;
    }

    public List<PlanoEnsino> getPlanosEnsino() {
        return planosEnsino;
    }

    public void setPlanosEnsino(List<PlanoEnsino> planosEnsino) {
        this.planosEnsino = planosEnsino;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public Integer getCargaHorariaTotal() {
        return cargaHorariaTotal;
    }

    public void setCargaHorariaTotal(Integer cargaHorariaTotal) {
        this.cargaHorariaTotal = cargaHorariaTotal;
    }

    public Integer getCargaHorariaPratica() {
        return cargaHorariaPratica;
    }

    public void setCargaHorariaPratica(Integer cargaHorariaPratica) {
        this.cargaHorariaPratica = cargaHorariaPratica;
    }

    public Integer getCargaHorariaTeorica() {
        return cargaHorariaTeorica;
    }

    public void setCargaHorariaTeorica(Integer cargaHorariaTeorica) {
        this.cargaHorariaTeorica = cargaHorariaTeorica;
    }

    public String getEmenta() {
        return ementa;
    }

    public void setEmenta(String ementa) {
        this.ementa = ementa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<CalendarioDisciplina> getHorarios() {
        return horarios;
    }

    public void setHorarios(List<CalendarioDisciplina> horarios) {
        this.horarios = horarios;
    }

    public List<CalendarioDocente> getHorariosDocentes() {
        return horariosDocentes;
    }

    public void setHorariosDocentes(List<CalendarioDocente> horariosDocentes) {
        this.horariosDocentes = horariosDocentes;
    }

    public List<CalendarioDiscente> getHorariosDiscentes() {
        return horariosDiscentes;
    }

    public void setHorariosDiscentes(List<CalendarioDiscente> horariosDiscentes) {
        this.horariosDiscentes = horariosDiscentes;
    }

    public boolean isAtiva() {
        return ativa;
    }

    public void setAtiva(boolean ativa) {
        this.ativa = ativa;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Disciplina other = (Disciplina) obj;
        return Objects.equals(this.codigo, other.codigo);
    }
}
