package br.edu.catolicato.sigeplane.domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by filipe on 26/06/2015.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipo")
public class Usuario implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    private String nome;

    @Transient
    private String primeiroNome;

    private String telefone;

    private String linkFacebook;

    private String linkTwitter;

    private String linkGPlus;

    private String linkGithub;

    @NotBlank
    @Column(unique = true)
    private String matricula;

    @NotBlank
    private String senha;

    @Transient
    private String senhaConfirmacao;

    @NotBlank
    @Column(unique = true)
    private String email;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Perfil perfil;

    @Column(name = "perdeu_senha")
    private boolean perdeuSenha;

    @OneToOne
    private Arquivo fotoPerfil;

    public Usuario() {
    }

    public Usuario(String nome, String matricula, String senha, String senhaConfirmacao,
                   String email, Perfil perfil, boolean perdeuSenha) {
        this.nome = nome;
        this.matricula = matricula;
        this.senha = senha;
        this.senhaConfirmacao = senhaConfirmacao;
        this.email = email;
        this.perfil = perfil;
        this.perdeuSenha = perdeuSenha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getSenhaConfirmacao() {
        return senhaConfirmacao;
    }

    public void setSenhaConfirmacao(String senhaConfirmacao) {
        this.senhaConfirmacao = senhaConfirmacao;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPrimeiroNome() {
        if (nome != null && nome.trim().length() > 0) {
            primeiroNome = nome.split(" ")[0];
        }
        return primeiroNome;
    }

    public void setPrimeiroNome(String primeiroNome) {
        this.primeiroNome = primeiroNome;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public boolean isPerdeuSenha() {
        return perdeuSenha;
    }

    public void setPerdeuSenha(boolean perdeuSenha) {
        this.perdeuSenha = perdeuSenha;
    }

    public Arquivo getFotoPerfil() {
        return fotoPerfil;
    }

    public void setFotoPerfil(Arquivo fotoPerfil) {
        this.fotoPerfil = fotoPerfil;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getLinkFacebook() {
        return linkFacebook;
    }

    public void setLinkFacebook(String linkFacebook) {
        this.linkFacebook = linkFacebook;
    }

    public String getLinkTwitter() {
        return linkTwitter;
    }

    public void setLinkTwitter(String linkTwitter) {
        this.linkTwitter = linkTwitter;
    }

    public String getLinkGPlus() {
        return linkGPlus;
    }

    public void setLinkGPlus(String linkGPlus) {
        this.linkGPlus = linkGPlus;
    }

    public String getLinkGithub() {
        return linkGithub;
    }

    public void setLinkGithub(String linkGithub) {
        this.linkGithub = linkGithub;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return Objects.equals(getId(), usuario.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
