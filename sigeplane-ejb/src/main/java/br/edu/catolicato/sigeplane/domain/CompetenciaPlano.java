package br.edu.catolicato.sigeplane.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Criado por Filipe D. Abreu em 02 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Entity
public class CompetenciaPlano implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    private Integer id;

    @ManyToOne
    private Competencia competencia;

    @OneToMany
    private List<HabilidadePlano> habilidades;

    @ManyToOne
    private PlanoEnsino planoEnsino;

    public CompetenciaPlano() {
        this.habilidades = new LinkedList<>();
    }

    public CompetenciaPlano(Competencia competencia, PlanoEnsino planoEnsino) {
        this.competencia = competencia;
        this.habilidades = new LinkedList<>();
        this.planoEnsino = planoEnsino;
    }

    public void addHabilidade(HabilidadePlano pHabilidade) {
        this.habilidades.add(pHabilidade);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<HabilidadePlano> getHabilidades() {
        return habilidades;
    }

    public void setHabilidades(List<HabilidadePlano> habilidades) {
        this.habilidades = habilidades;
    }

    public PlanoEnsino getPlanoEnsino() {
        return planoEnsino;
    }

    public void setPlanoEnsino(PlanoEnsino planoEnsino) {
        this.planoEnsino = planoEnsino;
    }

    public Competencia getCompetencia() {
        return competencia;
    }

    public void setCompetencia(Competencia competencia) {
        this.competencia = competencia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompetenciaPlano that = (CompetenciaPlano) o;
        return Objects.equals(getCompetencia(), that.getCompetencia()) &&
                Objects.equals(getPlanoEnsino(), that.getPlanoEnsino());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCompetencia(), getPlanoEnsino());
    }
}
