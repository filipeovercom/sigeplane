package br.edu.catolicato.sigeplane.servicos.seguranca;

import br.edu.catolicato.sigeplane.domain.Usuario;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;

import javax.inject.Named;
import java.io.Serializable;

/**
 * Criado por Filipe D. Abreu em 28 de Junho de 2015. SIGEPLANE - Sistema de
 * Gestão de Plano de Ensino Faculdade Católica do Tocantins - Sistemas de
 * Informação
 */
@Named
public class SegurancaService implements Serializable {

    private static final long serialVersionUID = 1L;


    private Repositorio repositorio;

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    public Usuario findUsuarioByMatricula(String matricula) {
        return repositorio.executeQuerySingleResult(Usuario.class, "SELECT u FROM Usuario u WHERE u.matricula = ?1", matricula);
    }

    public Usuario findUsuarioByEmail(String email) {
        return repositorio.executeQuerySingleResult(Usuario.class, "SELECT u FROM Usuario u WHERE u.email = ?1", email);
    }
}
