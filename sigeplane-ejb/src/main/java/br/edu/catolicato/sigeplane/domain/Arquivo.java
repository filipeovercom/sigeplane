package br.edu.catolicato.sigeplane.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Criado por Filipe D. Abreu em 27 de Junho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Católica do Tocantins - Sistemas de Informação
 */
@Entity
public class Arquivo implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nomeApresentacao;
    private String nomeArquivo;
    private String mimetype;
    private long tamanho;
    @Enumerated(EnumType.STRING)
    private TipoArquivo tipo;
    private String diretorio;
    @Temporal(TemporalType.DATE)
    private Date uploadDate;
    @Temporal(TemporalType.TIME)
    private Date uploadHour;
    @ManyToOne(fetch = FetchType.LAZY)
    private Usuario autor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    public long getTamanho() {
        return tamanho;
    }

    public void setTamanho(long tamanho) {
        this.tamanho = tamanho;
    }

    public TipoArquivo getTipo() {
        return tipo;
    }

    public void setTipo(TipoArquivo tipo) {
        this.tipo = tipo;
    }

    public String getDiretorio() {
        return diretorio;
    }

    public void setDiretorio(String diretorio) {
        this.diretorio = diretorio;
    }

    public String getNomeApresentacao() {
        return nomeApresentacao;
    }

    public void setNomeApresentacao(String nomeApresentacao) {
        this.nomeApresentacao = nomeApresentacao;
    }

    public String getNomeArquivo() {
        return nomeArquivo;
    }

    public void setNomeArquivo(String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Date getUploadHour() {
        return uploadHour;
    }

    public void setUploadHour(Date uploadHour) {
        this.uploadHour = uploadHour;
    }

    public Usuario getAutor() {
        return autor;
    }

    public void setAutor(Usuario autor) {
        this.autor = autor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Arquivo arquivo = (Arquivo) o;
        return Objects.equals(getId(), arquivo.getId()) &&
                Objects.equals(getNomeApresentacao(), arquivo.getNomeApresentacao());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNomeApresentacao());
    }
}
