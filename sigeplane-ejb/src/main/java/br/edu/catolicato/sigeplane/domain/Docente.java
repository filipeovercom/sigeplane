package br.edu.catolicato.sigeplane.domain;


import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Criado por Filipe D. Abreu em 27 de Junho de 2015. SIGEPLANE - Sistema de
 * Gestão de Plano de Ensino Católica do Tocantins - Sistemas de Informação
 */
@Entity
@DiscriminatorValue(value = "Docente")
public class Docente extends Usuario implements Serializable {

    private static final long serialVersionUID = 1L;

    private String bacharelado;
    private String tecnologo;
    private String licenciatura;
    private String especializacao;
    private String mestrado;
    private String doutorado;

    @OneToMany(mappedBy = "docente")
    private List<CalendarioDocente> horarios;

    @OneToMany(mappedBy = "docente", cascade = CascadeType.ALL, orphanRemoval = true, targetEntity = PlanoEnsino.class)
    private List<PlanoEnsino> planosEnsino;

    public Docente(String nome, String matricula, String senha, String senhaConfirmacao, String email, Perfil perfil, boolean perdeuSenha) {
        super(nome, matricula, senha, senhaConfirmacao, email, perfil, perdeuSenha);
        this.horarios = new LinkedList<>();
    }

    public Docente() {
        super();
        this.horarios = new LinkedList<>();
    }

    public void addHorario(CalendarioDocente calendarioDocente) {
        this.horarios.add(calendarioDocente);
        calendarioDocente.setDocente(this);
    }

    public void addPlanoEnsino(PlanoEnsino pPlanoEnsino) {
        this.planosEnsino.add(pPlanoEnsino);
        pPlanoEnsino.setDocente(this);
    }

    public String getBacharelado() {
        return bacharelado;
    }

    public void setBacharelado(String bacharelado) {
        this.bacharelado = bacharelado;
    }

    public String getTecnologo() {
        return tecnologo;
    }

    public void setTecnologo(String tecnologo) {
        this.tecnologo = tecnologo;
    }

    public String getLicenciatura() {
        return licenciatura;
    }

    public void setLicenciatura(String licenciatura) {
        this.licenciatura = licenciatura;
    }

    public String getEspecializacao() {
        return especializacao;
    }

    public void setEspecializacao(String especializacao) {
        this.especializacao = especializacao;
    }

    public String getMestrado() {
        return mestrado;
    }

    public void setMestrado(String mestrado) {
        this.mestrado = mestrado;
    }

    public String getDoutorado() {
        return doutorado;
    }

    public void setDoutorado(String doutorado) {
        this.doutorado = doutorado;
    }

    public List<CalendarioDocente> getHorarios() {
        return horarios;
    }

    public void setHorarios(List<CalendarioDocente> horarios) {
        this.horarios = horarios;
    }

    public List<PlanoEnsino> getPlanosEnsino() {
        return planosEnsino;
    }

    public void setPlanosEnsino(List<PlanoEnsino> planosEnsino) {
        this.planosEnsino = planosEnsino;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Docente docente = (Docente) o;
        return Objects.equals(this.getId(), docente.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.getId());
    }
}
