package br.edu.catolicato.sigeplane.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 27 de Junho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Católica do Tocantins - Sistemas de Informação
 */
@Entity
@DiscriminatorValue(value = "Discente")
public class Discente extends Usuario {

    @OneToMany(mappedBy = "discente", orphanRemoval = true, targetEntity = CalendarioDiscente.class)
    private List<CalendarioDiscente> horarios;

    public Discente(String nome, String matricula, String senha, String senhaConfirmacao, String email, Perfil perfil, boolean perdeuSenha) {
        super(nome, matricula, senha, senhaConfirmacao, email, perfil, perdeuSenha);
    }

    public Discente() {
        super();
    }

}
