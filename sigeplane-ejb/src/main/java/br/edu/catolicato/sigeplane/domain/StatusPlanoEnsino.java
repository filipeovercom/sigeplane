package br.edu.catolicato.sigeplane.domain;

/**
 * Criado por Filipe D. Abreu em 02 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public enum StatusPlanoEnsino {
    ABERTO("Aberto"), EM_ATUALIZACAO("Em Atualização"), AGUARDANDO_APROVACAO("Aguardando Aprovação"),
    REJEITADO("Rejeitado"), APROVADO("Aprovado"), FECHADO("Fechado");

    private final String descricao;

    StatusPlanoEnsino(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
