package br.edu.catolicato.sigeplane.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Criado por Filipe D. Abreu em 02 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Entity
public class AulaCalendario implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message = "Dia da Semana é obrigatória!")
    @Enumerated(EnumType.STRING)
    private DiaSemana diaSemana;

    @NotNull(message = "Data é obrigatória!")
    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date dataAula;

    @NotNull(message = "Tipo da Aula é obrigatório!")
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TipoAula tipoAula;

    @ManyToOne(fetch = FetchType.LAZY)
    private CalendarioAcademico calendarioAcademico;

    public AulaCalendario() {
    }

    public AulaCalendario(DiaSemana diaSemana, Date data, TipoAula tipoAula) {
        this.diaSemana = diaSemana;
        this.dataAula = data;
        this.tipoAula = tipoAula;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DiaSemana getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(DiaSemana diaSemana) {
        this.diaSemana = diaSemana;
    }

    public Date getDataAula() {
        return dataAula;
    }

    public void setDataAula(Date dataAula) {
        this.dataAula = dataAula;
    }

    public TipoAula getTipoAula() {
        return tipoAula;
    }

    public void setTipoAula(TipoAula tipoAula) {
        this.tipoAula = tipoAula;
    }

    public CalendarioAcademico getCalendarioAcademico() {
        return calendarioAcademico;
    }

    public void setCalendarioAcademico(CalendarioAcademico calendarioAcademico) {
        this.calendarioAcademico = calendarioAcademico;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AulaCalendario that = (AulaCalendario) o;
        return Objects.equals(getDiaSemana(), that.getDiaSemana()) &&
                Objects.equals(getDataAula(), that.getDataAula()) &&
                Objects.equals(getTipoAula(), that.getTipoAula());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDiaSemana(), getDataAula(), getTipoAula());
    }
}
