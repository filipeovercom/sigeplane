package br.edu.catolicato.sigeplane.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Criado por Filipe D. Abreu em 21 de Agosto de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Entity
public class CalendarioDiscente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message = "Calendário Acadêmico é obrigatório!")
    @ManyToOne
    private CalendarioAcademico calendarioAcademico;

    @NotNull(message = "Docente é obrigatório!")
    @ManyToOne
    private Discente discente;

    @ManyToMany
    private List<Disciplina> disciplinas;

    public CalendarioDiscente() {
        this.disciplinas = new LinkedList<>();
    }

    public CalendarioDiscente(CalendarioAcademico calendarioAcademico, Discente discente) {
        this.calendarioAcademico = calendarioAcademico;
        this.discente = discente;
        this.disciplinas = new LinkedList<>();
    }

    public void addDisciplina(Disciplina pDisciplina) {
        this.disciplinas.add(pDisciplina);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CalendarioAcademico getCalendarioAcademico() {
        return calendarioAcademico;
    }

    public void setCalendarioAcademico(CalendarioAcademico semestre) {
        this.calendarioAcademico = semestre;
    }

    public Discente getDiscente() {
        return discente;
    }

    public void setDiscente(Discente discente) {
        this.discente = discente;
    }

    public List<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalendarioDiscente that = (CalendarioDiscente) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getCalendarioAcademico(), that.getCalendarioAcademico()) &&
                Objects.equals(getDiscente(), that.getDiscente());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCalendarioAcademico(), getDiscente());
    }
}
