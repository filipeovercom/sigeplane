package br.edu.catolicato.sigeplane.servicos.calendarioacademico;

import br.edu.catolicato.sigeplane.domain.CalendarioAcademico;
import br.edu.catolicato.sigeplane.domain.CalendarioDisciplina;
import br.edu.catolicato.sigeplane.domain.Disciplina;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;

import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 01 de Julho de 2015. SIGEPLANE - Sistema de
 * Gestão de Plano de Ensino Faculdade Católica do Tocantins - Sistemas de
 * Informação
 */
public class CalendarioDisciplinaService implements Serializable {

    private static final long serialVersionUID = 1L;

    private Repositorio repositorio;

    public void setRepositorio(Repositorio pRepositorio) {
        this.repositorio = pRepositorio;
    }

    /*TRANSAÇÕES BÁSICAS*/
    public boolean salvarHorario(CalendarioDisciplina pHorarioDisciplina) {
        return repositorio.salvar(pHorarioDisciplina);
    }

    public boolean salvarHorarios(List<CalendarioDisciplina> listaHorario) {
        return repositorio.salvarLista(listaHorario);
    }

    public boolean editarHorario(CalendarioDisciplina pHorarioDisciplina) {
        return repositorio.editar(pHorarioDisciplina);
    }

    public boolean excluirHorario(CalendarioDisciplina pHorarioDisciplina) {
        return repositorio.excluir(pHorarioDisciplina);
    }

    public boolean excluirHorarios(List<CalendarioDisciplina> disciplinas) {
        return repositorio.excluirLista(disciplinas);
    }

    /*BUSCAS*/
    public List<CalendarioDisciplina> buscaTodos() {
        return repositorio.buscaTodos(CalendarioDisciplina.class);
    }

    public CalendarioDisciplina buscaPorId(Integer id) {
        return repositorio.buscaPorId(CalendarioDisciplina.class, id);
    }

    public CalendarioDisciplina findHrByDisciCalend(Disciplina disciplina, CalendarioAcademico calendarioAcademico) {
        return repositorio.executeQuerySingleResult(CalendarioDisciplina.class,
                "select h from CalendarioDisciplina h " +
                        "where h.calendarioAcademico = ?1 and h.disciplina = ?2", calendarioAcademico, disciplina);
    }

    public List<Disciplina> findDisciplinasWithCalendario(CalendarioAcademico pCalendarioAcademico) {
        return repositorio.executeQueryMultipleResult(Disciplina.class,
                "select d.disciplina from CalendarioDisciplina d " +
                        "where d.calendarioAcademico = ?1", pCalendarioAcademico);
    }
}
