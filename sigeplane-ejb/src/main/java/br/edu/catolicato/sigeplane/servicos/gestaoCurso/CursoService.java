package br.edu.catolicato.sigeplane.servicos.gestaoCurso;

import br.edu.catolicato.sigeplane.domain.Curso;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;

import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 17 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class CursoService implements Serializable {
    private static final long serialVersionUID = 1L;

    private Repositorio repositorio;

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    public boolean salvar(Curso curso) {
        return repositorio.salvar(curso);
    }

    public boolean editar(Curso curso) {
        return repositorio.editar(curso);
    }

    public boolean excluir(Curso curso) {
        return repositorio.excluir(curso);
    }

    public boolean salvarCursos(List<Curso> pCursos) {
        return repositorio.salvarLista(pCursos);
    }

    public boolean editarCursos(List<Curso> pCursos) {
        return repositorio.editarLista(pCursos);
    }

    /*********************** BUSCAS *************************/

    /**
     * Busca o curso pelo código
     *
     * @param codigo - String
     * @return Curso
     */
    public Curso findCursoByCodigo(String codigo) {
        return repositorio.executeQuerySingleResult(Curso.class, "select c from Curso c where c.codigo = ?1", codigo);
    }

    /**
     * Busca o curso pelo nome
     *
     * @param nome - String
     * @return Curso
     */
    public Curso findCursoByNome(String nome) {
        return repositorio.executeQuerySingleResult(Curso.class, "select c from Curso c where c.nome = ?1", nome);
    }

    /**
     * Busca todos os cursos sem os atributos lazy.
     *
     * @return Lista de Curso
     */
    public List<Curso> buscaTodos() {
        return repositorio.buscaTodos(Curso.class);
    }

    public Curso buscaPorId(Integer id) {
        return (Curso) repositorio.buscaPorId(Curso.class, id);
    }

}
