package br.edu.catolicato.sigeplane.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Criado por Filipe D. Abreu em 28 de Junho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Entity
public class HabilidadePlano implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    private Integer id;
    @OneToOne
    private Habilidade habilidade;
    @OneToMany
    private List<Avaliacao> avaliacao;
    @OneToMany
    private List<Conteudo> conteudos;
    @OneToMany
    private List<Metodologia> metodologias;
    @OneToMany
    private List<Recurso> recursos;
    @OneToMany
    private List<ReservaLaboratorio> laboratorios;
    @OneToMany
    private List<DataHabPlanoEnsino> datas;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Habilidade getHabilidade() {
        return habilidade;
    }

    public void setHabilidade(Habilidade habilidade) {
        this.habilidade = habilidade;
    }

    public List<Avaliacao> getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(List<Avaliacao> avaliacao) {
        this.avaliacao = avaliacao;
    }

    public List<Conteudo> getConteudos() {
        return conteudos;
    }

    public void setConteudos(List<Conteudo> conteudos) {
        this.conteudos = conteudos;
    }

    public List<Metodologia> getMetodologias() {
        return metodologias;
    }

    public void setMetodologias(List<Metodologia> metodologias) {
        this.metodologias = metodologias;
    }

    public List<Recurso> getRecursos() {
        return recursos;
    }

    public void setRecursos(List<Recurso> recursos) {
        this.recursos = recursos;
    }

    public List<ReservaLaboratorio> getLaboratorios() {
        return laboratorios;
    }

    public void setLaboratorios(List<ReservaLaboratorio> laboratorios) {
        this.laboratorios = laboratorios;
    }

    public List<DataHabPlanoEnsino> getDatas() {
        return datas;
    }

    public void setDatas(List<DataHabPlanoEnsino> datas) {
        this.datas = datas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HabilidadePlano that = (HabilidadePlano) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
