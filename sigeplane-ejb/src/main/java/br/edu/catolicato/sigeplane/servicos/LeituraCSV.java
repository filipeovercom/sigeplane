package br.edu.catolicato.sigeplane.servicos;

import org.primefaces.model.UploadedFile;

import java.util.List;

/**
 * Criado por Filipe D. Abreu em 06 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public interface LeituraCSV<T> {
    /**
     * <b>Método que faz a leitura do arquivo CSV passado
     * por parâmetro.</b>
     * @param pFile (UploadedFile) - Arquivo a ser lido.
     * @return List com os objetos lidos do arquivo.
     */
    public List<T> lerArquivo(UploadedFile pFile);

    /**
     * <b>Método que salva os objetos lidos do arquivo CSV
     * no banco de dados.</b>
     * @return boolean - True caso os dados tenham sido salvos
     */
    public boolean salvarDados();

}
