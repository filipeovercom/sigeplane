package br.edu.catolicato.sigeplane.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Criado por Filipe D. Abreu em 02 de Julho de 2015. SIGEPLANE - Sistema de
 * Gestão de Plano de Ensino Faculdade Católica do Tocantins - Sistemas de
 * Informação
 */
@Entity
public class DataReverva implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Temporal(TemporalType.DATE)
    private Date dataReserva;

    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = Turno.class)
    private List<Turno> turnos;

    public DataReverva() {
        this.turnos = new LinkedList<>();
    }

    public DataReverva(Date data, List<Turno> turnos) {
        this.dataReserva = data;
        this.turnos = turnos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataReserva() {
        return dataReserva;
    }

    public void setDataReserva(Date dataReserva) {
        this.dataReserva = dataReserva;
    }

    public List<Turno> getTurnos() {
        return turnos;
    }

    public void setTurnos(List<Turno> turnos) {
        this.turnos = turnos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataReverva that = (DataReverva) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
