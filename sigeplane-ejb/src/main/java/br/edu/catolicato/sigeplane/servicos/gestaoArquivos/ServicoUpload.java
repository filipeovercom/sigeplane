package br.edu.catolicato.sigeplane.servicos.gestaoArquivos;

import br.edu.catolicato.sigeplane.domain.Arquivo;
import br.edu.catolicato.sigeplane.domain.TipoArquivo;
import br.edu.catolicato.sigeplane.domain.Usuario;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;
import br.edu.catolicato.sigeplane.util.Utilidades;
import org.primefaces.model.UploadedFile;

import javax.ejb.EJB;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Criado por Filipe D. Abreu em 27 de Junho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Católica do Tocantins - Sistemas de Informação
 */
public class ServicoUpload implements Serializable {

    private static final long serialVersionUID = 1L;

    @EJB
    private Repositorio repositorio;

    public Arquivo geraArquivo(UploadedFile file, TipoArquivo tipoArquivo, Usuario criador) {
        Arquivo arquivo = new Arquivo();
        String fileName = Utilidades.removerAcentos(file.getFileName());
        arquivo.setNomeApresentacao(fileName);
        arquivo.setNomeArquivo(String.valueOf(arquivo.hashCode()) + "_" + fileName);
        arquivo.setTipo(tipoArquivo);
        arquivo.setDiretorio(Utilidades.getMediaPath(arquivo.getTipo()));
        arquivo.setTamanho(file.getSize());
        arquivo.setMimetype(file.getContentType());
        Date dataAtual = Calendar.getInstance().getTime();
        arquivo.setUploadDate(dataAtual);
        arquivo.setUploadHour(dataAtual);
        arquivo.setAutor(criador);
        return arquivo;
    }

    public boolean gravaArquivo(UploadedFile file, TipoArquivo tipoArquivo, Usuario criador) {
        if (file != null) {
            try {
                Arquivo arquivo = this.geraArquivo(file, tipoArquivo, criador);
                File outFile = new File(arquivo.getDiretorio() + arquivo.getNomeArquivo());
                byte[] byteData = file.getContents();
                File newFile = new File(arquivo.getDiretorio());
                if (!newFile.exists()) {
                    newFile.mkdirs();
                }
                FileOutputStream fos = new FileOutputStream(outFile);
                fos.write(byteData);
                fos.close();
                if (outFile.exists()) {
                    if (repositorio.salvar(arquivo)) {
                        return true;
                    }
                }
            } catch (Exception ex) {

            }
        }
        return false;
    }
}
