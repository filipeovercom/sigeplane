package br.edu.catolicato.sigeplane.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * Criado por Filipe D. Abreu em 7 de Setembro de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Entity
@DiscriminatorValue(value = "Administrator")
public class Administrator extends Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
}
