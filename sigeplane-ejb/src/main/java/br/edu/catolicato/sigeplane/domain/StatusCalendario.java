package br.edu.catolicato.sigeplane.domain;

/**
 * Criado por Filipe D. Abreu em 30 de Junho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public enum StatusCalendario {
    INICIADO("Iniciado"), EM_ANDAMENTO("Em Andamento"), FECHADO("Fechado");

    private final String descricao;

    StatusCalendario(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
