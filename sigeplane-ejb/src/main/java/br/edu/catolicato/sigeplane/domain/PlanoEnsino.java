package br.edu.catolicato.sigeplane.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Criado por Filipe D. Abreu em 28 de Junho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
@Entity
public class PlanoEnsino implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private StatusPlanoEnsino status;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private CalendarioAcademico calendarioAcademico;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Docente docente;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH},
            targetEntity = Disciplina.class)
    private Disciplina disciplina;

    private boolean guardaNumExibicoes;

    private Integer numExibicoes;

    @OneToMany(mappedBy = "planoEnsino")
    private List<CompetenciaPlano> competencias;

    @OneToMany
    private List<Livro> bibliografiaBasica;

    @OneToMany
    private List<Livro> bibliografiaComplementar;

    public PlanoEnsino() {
        this.competencias = new LinkedList<>();
        this.bibliografiaBasica = new LinkedList<>();
        this.bibliografiaComplementar = new LinkedList<>();
    }

    public PlanoEnsino(StatusPlanoEnsino status, CalendarioAcademico calendarioAcademico, Docente docente, Disciplina disciplina) {
        this.status = status;
        this.calendarioAcademico = calendarioAcademico;
        this.docente = docente;
        this.disciplina = disciplina;
        this.competencias = new LinkedList<>();
        this.bibliografiaBasica = new LinkedList<>();
        this.bibliografiaComplementar = new LinkedList<>();
    }

    public void addCompetencia(CompetenciaPlano pCompetenciaPlano) {
        this.competencias.add(pCompetenciaPlano);
        pCompetenciaPlano.setPlanoEnsino(this);
    }

    public void addBibliografiaBasica(Livro pLivro) {
        this.bibliografiaBasica.add(pLivro);
    }

    public void addBibliografiaComplementar(Livro pLivro) {
        this.bibliografiaComplementar.add(pLivro);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public StatusPlanoEnsino getStatus() {
        return status;
    }

    public void setStatus(StatusPlanoEnsino status) {
        this.status = status;
    }

    public CalendarioAcademico getCalendarioAcademico() {
        return calendarioAcademico;
    }

    public void setCalendarioAcademico(CalendarioAcademico calendarioAcademico) {
        this.calendarioAcademico = calendarioAcademico;
    }

    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    public List<CompetenciaPlano> getCompetencias() {
        return competencias;
    }

    public void setCompetencias(List<CompetenciaPlano> competencias) {
        this.competencias = competencias;
    }

    public List<Livro> getBibliografiaBasica() {
        return bibliografiaBasica;
    }

    public void setBibliografiaBasica(List<Livro> bibliografiaBasica) {
        this.bibliografiaBasica = bibliografiaBasica;
    }

    public List<Livro> getBibliografiaComplementar() {
        return bibliografiaComplementar;
    }

    public void setBibliografiaComplementar(List<Livro> bibliografiaComplementar) {
        this.bibliografiaComplementar = bibliografiaComplementar;
    }

    public Integer getNumExibicoes() {
        return numExibicoes;
    }

    public void setNumExibicoes(Integer numExibicoes) {
        this.numExibicoes = numExibicoes;
    }

    public boolean isGuardaNumExibicoes() {
        return guardaNumExibicoes;
    }

    public void setGuardaNumExibicoes(boolean guardaNumExibicoes) {
        this.guardaNumExibicoes = guardaNumExibicoes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlanoEnsino that = (PlanoEnsino) o;
        return Objects.equals(getCalendarioAcademico(), that.getCalendarioAcademico()) &&
                Objects.equals(getDocente(), that.getDocente()) &&
                Objects.equals(getDisciplina(), that.getDisciplina());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCalendarioAcademico(), getDocente(), getDisciplina());
    }
}
