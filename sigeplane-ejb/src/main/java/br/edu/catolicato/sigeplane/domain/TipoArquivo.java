package br.edu.catolicato.sigeplane.domain;

/**
 * Criado por Filipe D. Abreu em 27 de Junho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Católica do Tocantins - Sistemas de Informação
 */
public enum TipoArquivo {
    IMAGEM("Imagem"), PDF("Pdf"), EXCEL("Planilha Excel"), CSV("CSV(Arquivo separdo por vírgulas)"), VIDEO("Vídeo"), WORD("Documento do Word");

    private String descricao;

    TipoArquivo(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
