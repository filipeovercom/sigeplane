package br.edu.catolicato.sigeplane.servicos.calendarioacademico;

import br.edu.catolicato.sigeplane.domain.CalendarioAcademico;
import br.edu.catolicato.sigeplane.domain.CalendarioDocente;
import br.edu.catolicato.sigeplane.domain.Disciplina;
import br.edu.catolicato.sigeplane.domain.Docente;
import br.edu.catolicato.sigeplane.persistencia.Repositorio;

import java.io.Serializable;
import java.util.List;

/**
 * Criado por Filipe D. Abreu em 01 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public class CalendarioDocenteService implements Serializable {
    private static final long serialVersionUID = 1L;

    private Repositorio repositorio;

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    /*TRANSAÇÕES BÁSICAS*/
    public boolean salvarHorario(CalendarioDocente pCalendarioDocente) {
        return repositorio.salvar(pCalendarioDocente);
    }

    public boolean editarHorario(CalendarioDocente pCalendarioDocente) {
        return repositorio.editar(pCalendarioDocente);
    }

    public boolean excluirHorario(CalendarioDocente pCalendarioDocente) {
        return repositorio.excluir(pCalendarioDocente);
    }

    /*BUSCAS*/
    public List<CalendarioDocente> buscaTodos() {
        return repositorio.buscaTodos(CalendarioDocente.class);
    }

    public CalendarioDocente buscaPorId(Integer id) {
        return repositorio.buscaPorId(CalendarioDocente.class, id);
    }

    public CalendarioDocente findHrByDocenteCalendario(Docente pDocente, CalendarioAcademico calendarioAcademico) {
        return repositorio.executeQuerySingleResult(CalendarioDocente.class,
                "select h from CalendarioDocente h " +
                        "where h.docente = ?1 and h.calendarioAcademico = ?2", pDocente, calendarioAcademico);
    }

    public boolean salvarHorarios(List<CalendarioDocente> listaHorario) {
        return repositorio.salvarLista(listaHorario);
    }

    public Docente findProfComDiscEmCalend(CalendarioAcademico pCalendarioAcademico, Disciplina pDisciplina) {
        return repositorio.executeQuerySingleResult(Docente.class,
                "SELECT cd.docente FROM CalendarioDocente cd " +
                        "JOIN cd.disciplinas d " +
                        "WHERE cd.calendarioAcademico = ?1 " +
                        "AND d = ?2", pCalendarioAcademico, pDisciplina);
    }

    public List<Disciplina> findDisciplinasDisponiveis(CalendarioAcademico pCalendarioAcademico) {
        return repositorio.executeNativeQueryMultipleResult(Disciplina.class,
                "SELECT d.* FROM disciplina d " +
                        "INNER JOIN calendariodisciplina cd ON(cd.disciplina_id = d.id) " +
                        "WHERE cd.calendarioAcademico_id = ?1 AND (" +
                        "SELECT COUNT(cdd.horariosDocentes_id) FROM calendariodocente_disciplina cdd " +
                        "INNER JOIN calendariodocente ccd ON(ccd.id = cdd.horariosDocentes_id AND " +
                        "ccd.calendarioAcademico_id = ?2) " +
                        "WHERE cdd.disciplinas_id = d.id) = 0", pCalendarioAcademico, pCalendarioAcademico);
    }
}
