package br.edu.catolicato.sigeplane.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Criado por Filipe D. Abreu em 29 de Junho de 2015. SIGEPLANE - Sistema de
 * Gestão de Plano de Ensino Faculdade Católica do Tocantins - Sistemas de
 * Informação
 */
@Entity
public class CompetenciaDisciplina implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private Disciplina disciplina;

    @ManyToOne
    private Competencia competencia;

    @OneToMany
    private List<HabiCompeDisciplina> habilidades;

    public CompetenciaDisciplina() {
        this.habilidades = new LinkedList<>();
    }

    public CompetenciaDisciplina(Competencia competencia) {
        this.competencia = competencia;
        this.habilidades = new LinkedList<>();
    }

    public void addHabilidadeCompetencia(HabiCompeDisciplina pHabiCompeDisciplina) {
        this.habilidades.add(pHabiCompeDisciplina);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Competencia getCompetencia() {
        return competencia;
    }

    public void setCompetencia(Competencia competencia) {
        this.competencia = competencia;
    }

    public List<HabiCompeDisciplina> getHabilidades() {
        return habilidades;
    }

    public void setHabilidades(List<HabiCompeDisciplina> habilidades) {
        this.habilidades = habilidades;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CompetenciaDisciplina that = (CompetenciaDisciplina) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
