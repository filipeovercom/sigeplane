package br.edu.catolicato.sigeplane.domain;

/**
 * Criado por Filipe D. Abreu em 24 de Julho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Faculdade Católica do Tocantins - Sistemas de Informação
 */
public enum TipoAula {
    AULA_NORMAL("Aula Normal"), REPOSICAO("Reposicao"), EXAME_FINAL("Exame Final"), SUBSTITUTIVA("Substitutiva");

    private String descricao;

    TipoAula(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
