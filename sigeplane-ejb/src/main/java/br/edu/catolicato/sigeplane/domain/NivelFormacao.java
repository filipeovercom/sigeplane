package br.edu.catolicato.sigeplane.domain;

/**
 * Criado por Filipe D. Abreu em 27 de Junho de 2015.
 * SIGEPLANE - Sistema de Gestão de Plano de Ensino
 * Católica do Tocantins - Sistemas de Informação
 */
public enum NivelFormacao {
    TECNOLOGO("TECNOLOGO"), LICENCIATURA("LICENCIATURA"), BACHARELADO("BACHARELADO"), ESPECIALIZACAO("ESPECIALIZACAO"), MESTRADO("MESTRADO"), DOUTORADO("DOUTORADO");

    private String descricao;

    NivelFormacao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
